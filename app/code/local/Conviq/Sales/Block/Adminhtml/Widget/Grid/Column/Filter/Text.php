<?php

/**
 * Created by PhpStorm.
 * User: vlaz
 * Date: 10/6/2016
 * Time: 12:36 PM
 */
class Comviq_Sales_Block_Adminhtml_Widget_Grid_Column_Filter_Text extends Mage_Adminhtml_Block_Widget_Grid_Column_Filter_Text
{
    /**
     * Custom filter condition on
     * empty value in column OR not empty
     *
     * @return array
     */
    public function getCondition()
    {
        $customNotEmptyExp = '!empty';
        $customEmptyExp = 'empty';

        $filerValue = $this->getValue();
        if (strstr($filerValue, $customNotEmptyExp)) {
            return array('neq' => '');
        } elseif (strstr($filerValue, $customEmptyExp)) {
            return array(
                array('eq' => ''),
                array('null' => true)
            );
        } else {
            return parent::getCondition();
        }
    }
}