<?php

class Comviq_Sales_Block_Adminhtml_Sales_Order_View extends Mage_Adminhtml_Block_Sales_Order_View
{

    public function __construct()
    {
        parent::__construct();
        $this->_addButton('order_sendtoss4', array(
            'label'     => Mage::helper('sales')->__('Send to SS4'),
            'onclick'   => 'setLocation(\'' . $this->getSendToSs4Url() . '\')',
        ));
    }

    public function getSendToSs4Url()
    {
        return $this->getUrl('*/*/sendOrderToSs4');
    }

}
