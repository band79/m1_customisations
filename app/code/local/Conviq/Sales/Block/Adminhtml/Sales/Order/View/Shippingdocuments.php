<?php

class Comviq_Sales_Block_Adminhtml_Sales_Order_View_Shippingdocuments
    extends Mage_Adminhtml_Block_Sales_Order_View_Tab_Info
{

    /**
     * Getting Shipping Order Documents Collection
     *
     * @return mixed
     */
    public function getOrderDocuments()
    {
        $order = $this->getOrder();
        if ($order->getShippingDocuments()) {
            $documents = explode(',', $order->getShippingDocuments());
            $documents = array_unique($documents);
            $orderDocuments = Mage::getModel('comviq_shippingdocument/shippingdocument')
                ->getCollection()
                ->addFieldToFilter('name', array('in' => $documents));
            return $orderDocuments;
        }
        return false;
    }
}
