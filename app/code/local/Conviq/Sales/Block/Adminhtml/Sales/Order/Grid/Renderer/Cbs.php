<?php
/**
 * Cbs information in order grid
 *
 * @category   Comviq
 * @package    Comviq_Sales
 */
class Comviq_Sales_Block_Adminhtml_Sales_Order_Grid_Renderer_Cbs
        extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * Renders grid column
     *
     * @param   Varien_Object $row
     * @return  string
     */
    public function render(Varien_Object $row)
    {
        $date = $row->getData($this->getColumn()->getId());
        return $date ? 'Validated' : 'Not Validated';
    }
}
