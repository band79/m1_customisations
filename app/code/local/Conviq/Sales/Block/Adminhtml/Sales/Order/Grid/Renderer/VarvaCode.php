<?php
class Comviq_Sales_Block_Adminhtml_Sales_Order_Grid_Renderer_VarvaCode
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $value =  $row->getData($this->getColumn()->getIndex());
        if ($value) {
            $offerData = unserialize($value);
            if (is_array($offerData) &&
                isset($offerData['varva']['campaign_code']) &&
                !empty($offerData['varva']['campaign_code'])
            ) {
                return $offerData['varva']['campaign_code'];
            }
        }
        return null;
    }
}
