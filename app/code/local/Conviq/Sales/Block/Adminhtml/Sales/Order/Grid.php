<?php
class Comviq_Sales_Block_Adminhtml_Sales_Order_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid
{
    /* @var Comviq_Sales_Helper_Data */
    protected $_helper;
    public function __construct()
    {
        parent::__construct();
        $this->_helper = Mage::helper('comviq_sales');
    }

    protected function _prepareColumns()
    {
        parent::_prepareColumns();

        $this->_addOrderColumns();
        $this->_addSs4Columns();
        $this->_addOfferColumns();

        $this->sortColumnsByOrder();
        return $this;
    }

    protected function _addOrderColumns()
    {
        $this->addColumnAfter(
            'ssn',
            array(
                'header' => $this->_helper->__('ssn'),
                'index' => 'ssn',
            ),
            'created_at'
        );

        $this->addColumnAfter(
            'child_ssn',
            array(
                'header' => $this->_helper->__('Children (below 18)'),
                'index' => 'child_ssn',
            ),
            'ssn'
        );

        $this->addColumnAfter(
            'customer_email',
            array(
                'header' => $this->_helper->__('Email'),
                'width' => '100px',
                'index' => 'customer_email',
            ),
            'shipping_name'
        );

        $this->addColumnAfter(
            'shipping_method',
            array(
                'header' => $this->_helper->__('Shipping Method'),
                'index' => 'shipping_method',
                'type'  => 'options',
                'width' => '150px',
                'options' => $this->_getShippingMethods(),
            ),
            'activation_type'
        );

        $this->addColumnAfter(
            'payment_method',
            array(
                'header' => Mage::helper('sales')->__('Payment Method'),
                'index' => 'payment_method_code',
                'filter_index' => 'payment_method_code',
                'type'  => 'options',
                'width' => '70px',
                'options' => $this->_getPaymentMethods(),
            ),
            'shipping_method'
        );

        $this->addColumnAfter(
            'shipping_documents',
            array(
                'header' => $this->_helper->__('Shipping Documents'),
                'width' => '100px',
                'index' => 'shipping_documents',
                'renderer' => 'Comviq_Sales_Block_Adminhtml_Sales_Order_Grid_Renderer_Documents',
            ),
            'activation_type'
        );
        $this->addColumnAfter(
            'shipping_documents',
            array(
                'header' => $this->_helper->__('Customer IP'),
                'width' => '100px',
                'index' => 'customer_ip',
            ),
            'customer IP'
        );
    }

    protected function _addSs4Columns()
    {
        $this->addColumnAfter(
            'reseller_id',
            array(
                'header' => $this->_helper->__('ResellerID'),
                'width' => '100px',
                'index' => 'reseller_id',
            ),
            'payment_method'
        );
        $this->addColumnAfter(
            'cuse_agent_id',
            array(
                'header' => $this->_helper->__('Cuse Agent Id'),
                'width' => '100px',
                'index' => 'cuse_agent_id',
            ),
            'reseller_id'
        );
        $this->addColumnAfter(
            'super_store_id',
            array(
                'header' => $this->_helper->__('SS4 ID'),
                'width' => '100px',
                'type'  => 'text',
                'index' => 'super_store_id',
            ),
            'real_order_id'
        );
        $this->addColumnAfter(
            'super_store_status',
            array(
                'header' => $this->_helper->__('SS4 status'),
                'width' => '100px',
                'type'  => 'text',
                'index' => 'super_store_status',
            ),
            'shipping_name'
        );
    }

    protected function _addOfferColumns()
    {
        $this->addColumnAfter(
            'coupon_code',
            array(
                'header' => $this->_helper->__('Coupon'),
                'width' => '30px',
                'type'  => 'input',
                'index' => 'coupon_code',
                'renderer' => 'Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Text',
            ),
            'cuse_agent_id'
        );
        $this->addColumnAfter(
            'sku',
            array(
                'header' => $this->_helper->__('SKU'),
                'width' => '50px',
                'index' => 'sku',
                'filter_index' => 'main_table.sku',
            ),
            'coupon_code'
        );
        $this->addColumnAfter(
            'devices',
            array(
                'header' => $this->_helper->__('Devices'),
                'width' => '100px',
                'index' => 'devices',
                'renderer' => 'Comviq_Sales_Block_Adminhtml_Sales_Order_Grid_Renderer_Format',
            ),
            'sku'
        );
        $this->addColumnAfter(
            'subscriptions',
            array(
                'header' => $this->_helper->__('Subscriptions'),
                'width' => '100px',
                'index' => 'subscriptions',
                'renderer' => 'Comviq_Sales_Block_Adminhtml_Sales_Order_Grid_Renderer_Format',
            ),
            'devices'
        );
        $this->addColumnAfter(
            'previous_subscription_type',
            array(
                'header' => $this->_helper->__('Previous Subscriptions'),
                'width' => '100px',
                'index' => 'previous_subscription_type',
                'renderer' => 'Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Text',
            ),
            'devices'
        );
        $this->addColumnAfter(
            'previous_price_plan',
            array(
                'header' => $this->_helper->__('Previous Price Plan'),
                'width' => '100px',
                'index' => 'previous_price_plan',
                'renderer' => 'Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Text',
            ),
            'devices'
        );
        $this->addColumnAfter(
            'previous_discount_level',
            array(
                'header' => $this->_helper->__('Previous Discount Level'),
                'width' => '100px',
                'index' => 'previous_discount_level',
                'renderer' => 'Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Text',
            ),
            'devices'
        );
        $this->addColumnAfter(
            'is_validated_in_cbs',
            array(
                'header' => $this->_helper->__('CBS Validation'),
                'width' => '100px',
                'index' => 'is_validated_in_cbs',
                'renderer' => 'Comviq_Sales_Block_Adminhtml_Sales_Order_Grid_Renderer_Cbs',
                'type' => 'options',
                'options' => $this->_getCbsOptions(),
                'filter_condition_callback' => array($this, '_cbsValidationCondition'),
            ),
            'previous_subscription_type'
        );
        $this->addColumnAfter(
            'installment_period',
            array(
                'header' => $this->_helper->__('Installment binding period'),
                'width' => '100px',
                'index' => 'installment_period',
            ),
            'subscriptions'
        );
        $this->addColumnAfter(
            'installment_monthly_price',
            array(
                'header' => $this->_helper->__('Installment monthly payment'),
                'width' => '100px',
                'index' => 'installment_monthly_price',
            ),
            'installment_period'
        );
        $this->addColumnAfter(
            'phone_notification',
            array(
                'header' => $this->_helper->__('Phone'),
                'width' => '100px',
                'index' => 'phone_notification',
            ),
            'customer_email'
        );
        $this->addColumnAfter(
            'tel_number',
            array(
                'header' => $this->_helper->__('Telephone Number'),
                'width' => '100px',
                'index' => 'tel_number',
            ),
            'devices'
        );
        $this->addColumnAfter(
            'activation_type',
            array(
                'header' => $this->_helper->__('Activation Type'),
                'width' => '100px',
                'index' => 'activation_type',
            ),
            'tel_number'
        );
        $this->addColumnAfter(
            'subscription_name',
            array(
                'header' => $this->_helper->__('Subscription Name'),
                'width' => '100px',
                'index' => 'subscription_name',
            ),
            'activation_type'
        );
        $this->addColumnAfter(
            'is_downgrade',
            array(
                'header' => $this->_helper->__('Is Downgrade'),
                'type' => 'options',
                'width' => '100px',
                'index' => 'is_downgrade',
                'options' => array(
                                '1' => $this->_helper->__('Yes'),
                                '0' => $this->_helper->__('No'),
                            ),
                'filter_condition_callback' => array($this, '_downgradeValidationCondition'),
            ),
            'activation_type'
        );
        $this->addColumnAfter(
            'insurance',
            array(
                'header' => $this->_helper->__('Insurance'),
                'width' => '100px',
                'index' => 'insurance',
            ),
            'is_downgrade'
        );
    }

    protected function _cbsValidationCondition($collection, $column)
    {
        $value = $column->getFilter()->getValue();
        if ($value == 0) {
            $collection->addFieldToFilter('is_validated_in_cbs', array('null' => true));
        } elseif ($value == 1) {
            $collection->addFieldToFilter('is_validated_in_cbs', array('eq' => 1));
        }
        return $this;
    }

    protected function _downgradeValidationCondition($collection, $column)
    {
        $value = $column->getFilter()->getValue();
        if ($value == 0) {
            $collection->addFieldToFilter('is_downgrade', array('null' => true));
        } elseif ($value == 1) {
            $collection->addFieldToFilter('is_downgrade', array('eq' => 1));
        }
        return $this;
    }

    protected function _getPaymentMethods()
    {
        $methods = array();

        //if we switch a method of, we'll be unable to see its name in the grid. the same with shipping method
        $payments = Mage::getSingleton('payment/config')->getActiveMethods();
        foreach ($payments as $paymentCode => $paymentModel) {
            $paymentTitle = Mage::getStoreConfig('payment/'.$paymentCode.'/title');
            $methods[$paymentCode] = $paymentTitle;
        }
        return $methods;
    }

    /**
     * Getting all active carriers (shipping method)
     * @return array
     */
    protected function _getShippingMethods()
    {
        $methods = array();
        $carriers = Mage::getSingleton('shipping/config')->getActiveCarriers();
        foreach ($carriers as $shippingCode => $shippingModel) {
            $shippingTitle = Mage::getStoreConfig('carriers/'.$shippingCode.'/title');
            $methods[$shippingCode . '_' . $shippingCode] = $shippingTitle;
        }
        return $methods;
    }

    /**
     * @return array
     */
    protected function _getCbsOptions()
    {
        return array(1 => 'Validated', 0 => 'Not Validated');
    }

    protected function _prepareMassaction()
    {
        parent::_prepareMassaction();
        $this->getMassactionBlock()->addItem('send_order_to_ss4', array(
            'label'=> Mage::helper('sales')->__('Send order to SS4'),
            'url'  => $this->getUrl('*/sales_order/massSendOrderToSs4'),
        ));
        return $this;
    }
}
