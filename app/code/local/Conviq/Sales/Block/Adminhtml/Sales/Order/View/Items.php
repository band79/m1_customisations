<?php

/**
 * Class Comviq_Sales_Block_Sales_Order_View_Items
 */

class Comviq_Sales_Block_Adminhtml_Sales_Order_View_Items
    extends Mage_Adminhtml_Block_Sales_Order_View_Items
{
    /**
     * Return Ordered Subscripiton Configs
     *
     * @return array
     */
    public function getSubscriptionConfigs()
    {
        $subscriptionConfigs = array();
        $orderItems = $this->getItemsCollection();
        foreach ($orderItems as $_item) {
            if ($configData = json_decode($_item->getSubscriptionConfigsData())) {
                $subscriptionConfigs = array_merge($subscriptionConfigs, (array)$configData);
            }
        }
        return $subscriptionConfigs;
    }
}
