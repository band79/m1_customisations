<?php
class Comviq_Sales_Block_Adminhtml_Sales_Order_Grid_Renderer_Format
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $value =  $row->getData($this->getColumn()->getIndex());
        if ($value) {
            $result = json_decode($value, true);
            if (is_array($result)) {
                return implode(",<br>", $result);
            }
        }
        return '';
    }
}