<?php
class Comviq_Sales_Block_Adminhtml_Sales_Order_Grid_Renderer_Items
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        /* @var $row Comviq_Sales_Model_Order */
        $items = $row->getItemsCollection(array(Mage_Catalog_Model_Product_Type::TYPE_SIMPLE));
        $str = '';
        foreach ($items as $_item) {
            $str .= $_item->getName() . '<br />';
            $str .= $_item->getTelNumber() . '<br />';
            $str .= $_item->getActivationType() . '<br />';
            $str .= $_item->getSubscriptionName() . '<br />';
        }

        $str .= $row->getBillingAddress()->getTelephone() . '<br />';
        $str .= $row->getBillingAddress()->getEmail();

        return $str;
    }
}