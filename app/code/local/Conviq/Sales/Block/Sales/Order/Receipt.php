<?php


class Comviq_Sales_Block_Sales_Order_Receipt extends Mage_Core_Block_Template
{
    const XML_PATH_PRINT_PAGE = 'comviq/order/print_template';

    protected $_aTypePort;
    protected $_aTypeNew;

    protected function _construct()
    {
        parent::_construct();

        $this->_aTypePort = Comviq_Checkout_Model_Activationtype::ACTIVATION_TYPE_PORT;
        $this->_aTypeNew = Comviq_Checkout_Model_Activationtype::ACTIVATION_TYPE_NEW;
    }

    /**
     * Prepare html output
     *
     * @return string
     */
    protected function _toHtml()
    {

        $catalogHelper = Mage::helper('comviq_catalog');
        $checkoutHelper = Mage::helper('comviq_checkout');
        $insuranceHelper = Mage::helper("comviq_insurance");
        $order = Mage::registry('current_order');
        $totalMonthlyPrice = $checkoutHelper->getTotalMonthlyPrice($order);

        $subscriptionItem = $checkoutHelper->getSubscriptionOwnerItemFromOrder($order);
        $monthlyPrice = round($subscriptionItem->getData('monthly_price'), 2);
        $simpleProduct = $subscriptionItem->getProduct();
        $isMobile = $catalogHelper->isMobile($simpleProduct);

        /** @var $template Mage_Core_Model_Email_Template */
        $template = Mage::getModel('core/email_template');
        $storeId = Mage::app()->getStore()->getId();

        $templateId = Mage::getStoreConfig(self::XML_PATH_PRINT_PAGE, $storeId);

        if (is_numeric($templateId)) {
            $template->load($templateId);
        } else {
            $localeCode = Mage::getStoreConfig('general/locale/code', $storeId);
            $template->loadDefault($templateId, $localeCode);
        }

        /* @var $filter Mage_Core_Model_Input_Filter_MaliciousCode */
        $filter = Mage::getSingleton('core/input_filter_maliciousCode');

        $template->setTemplateText(
            $filter->filter($template->getTemplateText())
        );
        $shippingMethodName = Mage::getModel("comviq_shipping/carrier_noshipping")->getShippingMethodName();
        $togoShippingName = Mage::getModel("comviq_togo/togo")->getShippingMethodName();
        $activationType = $subscriptionItem->getData('activation_type');

        $vars = array();
        $vars['order'] = $order;
        $vars['isMobile'] = $isMobile;
        $vars['productName'] = $this->escapeHtml($simpleProduct->getName());
        $vars['subscriptionName'] = $subscriptionItem->getData('subscription_name');
        $vars['price'] = array(
            'isMobile' => $isMobile,
            'monthlyPrice' => $monthlyPrice,
            'totalMonthlyPrice' => (int)$totalMonthlyPrice
        );
        $vars['telephoneNumber'] = $subscriptionItem->getData('tel_number');
        $vars['activationType'] = $activationType;
        $vars['isActivationTypeNew'] = $this->_isActivationTypeNew($activationType);
        $vars['alternativeExpectedDelivery'] = $this->_getAlternativeExpectedDelivery($order);
        $vars['expectedDeliveryTime'] = $subscriptionItem->getData('expected_delivery_time');
        $vars['orderNumber'] = $order->getSuperStoreId();
        $vars['orderDate'] = $this->_getFormatDate($order);
        $vars['isNoTogo'] = !$this->_isTogo($order, $togoShippingName);
        $vars['isTogo'] = $this->_isTogo($order, $togoShippingName);
        $vars['showShipping'] = $this->_showShipping($order, $shippingMethodName);
        $vars['isTogoAndActivationTypePort'] = $this->_isTogoAndTypePort($vars['isTogo'], $activationType);
        $vars['isTogoAndActivationTypeNoPort'] = $this->_isTogoAndTypeNoPort($vars['isTogo'], $activationType);
        $vars['showEmail'] = !(bool)Mage::helper('comviq_prolongcampaign')->isEmailHidden();
        if ($order->getInsuranceRule()) {
            $vars['insurance'] = $order->getInsurance();
            $vars['insurancePrice'] = $order->getInsuranceRule()->getInsurancePrice();
        }

        Mage::app()->getStore()->setId($order->getStoreId());
        $templateProcessed = $template->getProcessedTemplate($vars, true);
        Mage::app()->getStore()->setId($storeId);

        return $templateProcessed;
    }

    /**
     * @param $isTogo
     * @param $activationType
     * @return bool
     */
    protected function _isTogoAndTypeNoPort($isTogo, $activationType)
    {
        if ($isTogo && ($activationType != $this->_aTypePort)) {
            return true;
        }
        return false;
    }

    /**
     * @param $isTogo
     * @param $activationType
     * @return bool
     */
    protected function _isTogoAndTypePort($isTogo, $activationType)
    {
        if ($isTogo && ($activationType == $this->_aTypePort)) {
            return true;
        }
        return false;
    }

    /**
     * @param $order
     * @param $shippingMethodName
     * @return bool
     */
    protected function _showShipping($order, $shippingMethodName)
    {
        if ($order->getShippingMethod() != $shippingMethodName) {
            return true;
        }
        return false;
    }

    /**
     * @param $activationType
     * @return bool
     */
    protected function _isActivationTypeNew($activationType)
    {
        if ($activationType == $this->_aTypeNew) {
            return true;
        }
        return false;
    }

    /**
     * @param $order
     * @param $togoShippingName
     * @return bool
     */
    protected function _isTogo($order, $togoShippingName)
    {
        if ($order->getShippingMethod() == $togoShippingName) {
            return true;
        }
        return false;
    }

    /**
     * @param $order
     * @return mixed
     */
    protected function _getAlternativeExpectedDelivery($order)
    {
        return Mage::helper('comviq_sales')->getAlternativeExpectedDelivery($order);
    }

    /**
     * @param $order
     * @return mixed
     */
    protected function _getFormatDate($order)
    {
        return Mage::helper('core')->formatDate($order->getCreatedAtStoreDate(), 'medium', false);
    }
}
