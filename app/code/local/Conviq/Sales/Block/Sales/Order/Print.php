<?php
/**
 * Order information for print
 *
 * @category   Comviq
 * @package    Comviq_Sales
 */

class Comviq_Sales_Block_Sales_Order_Print extends Mage_Sales_Block_Order_Print
{
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($headBlock = $this->getLayout()->getBlock('head')) {
            $headBlock->setTitle($this->__('Print Order # %s', $this->getOrder()->getSuperStoreId()));
        }
    }
}