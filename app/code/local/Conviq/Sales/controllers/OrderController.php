<?php
/**
 * Hack to avoid authorization requirements in view order
 */
require ROOT_PATH.'app/code/core/Mage/Sales/controllers/OrderController.php';
class Comviq_Sales_OrderController extends Mage_Sales_OrderController
{

    protected function _canViewOrder($order)
    {
        $sesId = !empty($_COOKIE['adminhtml']) ? $_COOKIE['adminhtml'] : false ;
        if ($sesId) {
            $session = Mage::getSingleton('core/resource_session')->read($sesId);
            if (stristr($session, 'Mage_Admin_Model_User')) {
                return true;
            }
        }

        $successOrderId = Mage::getSingleton('customer/session')->getSuccessOrderId();
        if ($order->getId() != $successOrderId) {
            $this->getResponse()->setRedirect(Mage::getBaseUrl());
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
        }
        return true;
    }

    /**
     * Action predispatch
     *
     * Check customer authentication for some actions
     */
    public function preDispatch()
    {
        Mage_Core_Controller_Front_Action::preDispatch();

        $action = $this->getRequest()->getActionName();
        if ($action != 'print') {
            parent::preDispatch();
            $loginUrl = Mage::helper('customer')->getLoginUrl();

            if (!Mage::getSingleton('customer/session')->authenticate($this, $loginUrl)) {
                $this->setFlag('', self::FLAG_NO_DISPATCH, true);
            }
        }
    }

    public function printAction()
    {
        if (!$this->_loadValidOrder()) {
            return;
        }
        if (Mage::getStoreConfig('comviq/order/enabled')) {
            $this->loadLayout('receipt');
        } else {
            $this->loadLayout('print');
        }
        $this->renderLayout();
    }
}