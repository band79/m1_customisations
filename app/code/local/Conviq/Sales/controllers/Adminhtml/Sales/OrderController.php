<?php

require_once Mage::getModuleDir('controllers', 'Mage_Adminhtml') . DS . 'Sales/OrderController.php';

class Comviq_Sales_Adminhtml_Sales_OrderController extends Mage_Adminhtml_Sales_OrderController
{

    protected $_superStoreResponse = null;
    protected $_superStoreRequest = null;
    protected $_superStoreStatus = null;
    protected $_superStoreId = null;
    protected $_orderId = null;
    protected $_superStoreError = null;
    protected $_isTogo = false;

    /**
     * Send to Ss4 order
     */
    public function massSendOrderToSs4Action()
    {
        $orderIds = $this->getRequest()->getPost('order_ids', array());
        asort($orderIds);
        $countSentOrder = 0;

        $ss4 = Mage::getModel('comviq_sS4Integration/sS4OrderCreation');
        foreach ($orderIds as $orderId) {
            $order = Mage::getModel('sales/order')->load($orderId);
            $ss4->createOrder($order);
            $countSentOrder++;
        }

        $countNonSentOrder = count($orderIds) - $countSentOrder;

        if ($countNonSentOrder) {
            if ($countSentOrder) {
                $this->_getSession()->addError($this->__('%s order(s) were not sent to ss4.', $countNonSentOrder));
            } else {
                $this->_getSession()->addError($this->__('No order(s) were sent to ss4.'));
            }
        }
        if ($countSentOrder) {
            $this->_getSession()->addSuccess($this->__('%s order(s) have been sent to ss4.', $countSentOrder));
        }

        $this->_redirect('*/*/');
    }

    /**
     * Send to Ss4 order
     */
    public function sendOrderToSs4Action()
    {
        if ($order = $this->_initOrder()) {
            try {
                $ss4 = Mage::getModel('comviq_sS4Integration/sS4OrderCreation');
                $ss4->createOrder($order);
                $this->_getSession()->addSuccess(
                    $this->__('The order has been sent to SS4.')
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('The order was not sent to ss4.'));
            }
            $this->_redirect('*/sales_order/view', array('order_id' => $order->getId()));
        }
    }
}
