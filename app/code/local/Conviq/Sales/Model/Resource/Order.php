<?php

class Comviq_Sales_Model_Resource_Order extends Mage_Sales_Model_Resource_Order
{
    /**
     * Returns Success Order Ids By SSN and Subsidy Id
     * @param $ssn string
     * @param $subsidyId int
     *
     * @return mixed
     */
    public function getSuccessOrderIdsBySsnAndSubsidy($ssn, $subsidyId)
    {
        $adapter = $this->getReadConnection();
        $select = $adapter->select()
            ->from(
                array('o' => $this->getTable('sales/order')),
                array('o.entity_id')
            )
            ->joinInner(
                array('i' => $this->getTable('sales/order_item')),
                'i.order_id=o.entity_id',
                array()
            )
            ->where('o.ssn=?', $ssn)
            ->where('i.subscription_id=?', $subsidyId)
            ->where('o.status=?', 'success');
        return $adapter->fetchAssoc($select);
    }

    /**
     * Returns Success Order Ids By Child's SSN and Subsidy Id
     * @param $ssn string
     * @param $subsidyId int
     *
     * @return mixed
     */
    public function getSuccessOrderIdsByChildSsnAndSubsidy($ssn, $subsidyId)
    {
        $adapter = $this->getReadConnection();
        $select = $adapter->select()
            ->from(
                array('o' => $this->getTable('sales/order')),
                array('o.entity_id')
            )
            ->joinInner(
                array('i' => $this->getTable('sales/order_item')),
                'i.order_id=o.entity_id',
                array()
            )
            ->where('o.child_ssn=?', $ssn)
            ->where('i.subscription_id=?', $subsidyId)
            ->where('o.status=?', 'success');
        return $adapter->fetchAssoc($select);
    }
}
