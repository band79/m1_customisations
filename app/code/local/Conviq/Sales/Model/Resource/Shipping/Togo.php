<?php

class Comviq_Sales_Model_Resource_Shipping_Togo
    extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Initialize resource
     *
     */
    protected function _construct()
    {
        $this->_init('comviq_togo/shipping', 'id');
    }
}
