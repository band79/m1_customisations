<?php
/**
 * Setup Model of Sales Module
 *
 * @category    Comviq
 * @package     Comviq_Sales
 */
class Comviq_Sales_Model_Resource_Setup extends Mage_Sales_Model_Resource_Setup
{

    /**
     * Add item (table) to The List of entities
     */
    public function addTableToFlatEntityTables($tableData)
    {
        $this->_flatEntityTables += $tableData;
    }

    /**
     * Remove entity attribute. Overwritten for flat entities support
     *
     * @param int|string $entityTypeId
     * @param string $code
     * @return Mage_Sales_Model_Resource_Setup
     */
    public function removeAttribute($entityTypeId, $code)
    {
        if (isset($this->_flatEntityTables[$entityTypeId]) &&
            $this->_flatTableExist($this->_flatEntityTables[$entityTypeId]))
        {
            $this->_removeFlatAttribute($this->_flatEntityTables[$entityTypeId], $code);
            $this->_removeGridAttribute($this->_flatEntityTables[$entityTypeId], $code, $entityTypeId);
        } else {
            parent::removeAttribute($entityTypeId, $code);
        }
        return $this;
    }

    /**
     * Remove an attribute as separate column in the table
     * The sales setup class doesn't support it by default
     *
     * @param string $table
     * @param string $attribute
     * @return Mage_Sales_Model_Resource_Setup
     */
    protected function _removeFlatAttribute($table, $attribute)
    {
        $this->getConnection()->dropColumn($this->getTable($table), $attribute);
        return $this;
    }

    /**
     * Remove attribute from grid
     *
     * @param string $table
     * @param string $attribute
     * @param string $entityTypeId
     * @return Mage_Sales_Model_Resource_Setup
     */
    protected function _removeGridAttribute($table, $attribute, $entityTypeId)
    {
        if (in_array($entityTypeId, $this->_flatEntitiesGrid)) {
            $this->getConnection()->dropColumn($this->getTable($table . '_grid'), $attribute);
        }
        return $this;
    }
}
