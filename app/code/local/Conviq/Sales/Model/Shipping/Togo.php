<?php

class Comviq_Sales_Model_Shipping_Togo extends Mage_Core_Model_Abstract
{
    /**
     * Constructor
     */
    protected function _construct()
    {
        $this->_init('comviq_sales/shipping_togo');
    }

    public function loadByQuoteId($quoteId = null)
    {
        if ($quoteId) {
            $this->load($quoteId, 'quote_id');
        }
        return $this;
    }
}