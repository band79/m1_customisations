<?php
/**
 * Magento Enterprise Edition
 *
 * @category   Comviq
 * @package    Comviq_Sales
 */

/**
 * Comviq Sales observer
 *
 */
class Comviq_Sales_Model_Observer extends Mage_Sales_Model_Observer
{
    protected $_gridBind;

    public function setCustomAttributes($observer)
    {
        $item = $observer->getQuoteItem();
        $product = $observer->getProduct();
        $item->setTogo($product->getTogo());
        return $this;
    }

    public function salesOrderAfterPlace($observer)
    {
        try {
            $order = $observer->getOrder();
            /* @var $ss4 Comviq_SS4Integration_Model_SS4OrderCreation */
            $ss4 = Mage::getModel('comviq_sS4Integration/sS4OrderCreation');
            $ss4->createOrder($order);
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    /**
     * @param $observer
     */
    public function ss4OrderSuccessful($observer)
    {
        $order = $observer->getOrder();

        $order->sendNewOrderEmail(true);

        $this->addVarvaCodeToComment($order);

        $this->createAccount($order);

        /* set false for credit check */
        if (Mage::getSingleton('checkout/session')->getSkipCreditCheck()) {
            /* set -1 for presmoker */
            $value = Mage::helper('comviq_comviq')->getConfig('comviq/downgrade/limit_presmoker');
            Mage::helper('comviq_comviq')->setConfig('comviq/downgrade/limit_presmoker', ($value - 1));
            Mage::getSingleton('checkout/session')->setSkipCreditCheck(false);
        }

        // remove Chosen New Number from AvailablePhoneNumbers
        /* @var Comviq_Checkout_Helper_Data $checkoutHelper */
        $checkoutHelper = Mage::helper('comviq_checkout');
        $subsOwnerItem = $checkoutHelper->getSubscriptionOwnerItemFromOrder($order);
        if ($subsOwnerItem->getData('activation_type') == Comviq_Checkout_Model_Activationtype::ACTIVATION_TYPE_NEW) {
            /* @var $ss4IntegrationHelper Comviq_SS4Integration_Helper_Data */
            $ss4IntegrationHelper = Mage::helper('comviq_sS4Integration');
            if ($ss4IntegrationHelper->removeChosenNumber($subsOwnerItem->getData('tel_number'))) {
                $ss4IntegrationHelper->addNewNumber();
            }
        }
    }

    /**
     * create Comviq Account
     *
     * @param $order
     */
    public function createAccount($order)
    {
        /** @var Comviq_SS4Integration_Model_Account $comviqAccount */
        $comviqAccount = Mage::getModel('comviq_sS4Integration/account');
        $response = $comviqAccount->createAccount($order);
        // TODO: check response on answer
        if ($response) {
            $order->addStatusHistoryComment(print_r($response, 1))->save();
        }
    }


    /**
     * Tell a friend campaign.
     * Add Varva code as comment to order.
     * 
     * @param Mage_Sales_Model_Order $order
     * @return \Comviq_Sales_Model_Observer
     */
    public function addVarvaCodeToComment($order)
    {
        /* @var Comviq_Checkout_Helper_Data $comviqCheckoutHelper */
        $comviqCheckoutHelper = Mage::helper('comviq_checkout');
        $item = $comviqCheckoutHelper->getSubscriptionOwnerItemFromOrder($order);
        if ($item->getData('varva_code')) {
            $order->addStatusHistoryComment($item->getData('varva_code'), false);
            $order->save();
        }

        return $this;
    }

    /**
     * Sends cancelOrder request to ss4
     *
     * @return Comviq_Sales_Model_Observer
     */
    public function cancelOrder()
    {
        $session = Mage::getSingleton('adminhtml/session');
        try {
            /* @var Comviq_Sales_Model_Order $order */
            $order = Mage::registry('current_order');
            if ($order) {
                $this->_cancelOrder($order);
            }
        } catch (Exception $e) {
            /* @var Comviq_SS4Integration_Helper_Data $ss4IntegrationHelper */
            $ss4IntegrationHelper = Mage::helper('comviq_sS4Integration');
            
            Mage::helper('comviq_log')->log(
                Comviq_Log_Helper_Data::CHECKOUT_MODULE,
                Comviq_Log_Helper_Data::ORDER,
                'cancelOrder',
                $e->getMessage(),
                Comviq_Log_Helper_Data::UN_SUCCESS
            );

            $session->addError($ss4IntegrationHelper->__($e->getMessage() . ' See ss4.cancelOrder.log for exception'));
        }

        return $this;
    }

    /**
     * Sends cancelOrder request to ss4 (one by one)
     *
     * @return Comviq_Sales_Model_Observer
     */
    public function massCancelOrder($event)
    {
        $session = Mage::getSingleton('adminhtml/session');
        try {
            /* @var Comviq_SS4Integration_Helper_Data $ss4IntegrationHelper */
            $ss4IntegrationHelper = Mage::helper('comviq_sS4Integration');
            $orderIds = $event->getControllerAction()->getRequest()->getPost('order_ids', array());
            foreach ($orderIds as $orderId) {
                /* @var Comviq_Sales_Model_Order $order */
                $order = Mage::getModel('sales/order')->load($orderId);
                $this->_cancelOrder($order);
            }
        } catch (Exception $e) {
            Mage::helper('comviq_log')->log(
                Comviq_Log_Helper_Data::CHECKOUT_MODULE,
                Comviq_Log_Helper_Data::ORDER,
                'cancelOrder',
                $e->getMessage(),
                Comviq_Log_Helper_Data::UN_SUCCESS
            );
            $session->addError($ss4IntegrationHelper->__($e->getMessage() . ' See ss4.cancelOrder.log for exception'));
        }

        return $this;
    }

    /**
     * cancel Order in the SS4
     *
     * @param Comviq_Sales_Model_Order $order
     *
     * @throws Exception
     */
    protected function _cancelOrder($order)
    {
        $session = Mage::getSingleton('adminhtml/session');
        $ss4OrderId = $order->getSuperStoreId();
        $wsdlType = $this->_getWsdlType($order);
        /* @var Comviq_SS4Integration_Helper_Data $ss4IntegrationHelper */
        $ss4IntegrationHelper = Mage::helper('comviq_sS4Integration');
        /* @var Comviq_Comviq_Helper_Data $comviqHelper */
        $comviqHelper = Mage::helper('comviq_comviq');

        $result = $ss4IntegrationHelper->cancelOrder($ss4OrderId, $wsdlType);
        $status = $comviqHelper->getDataFromSS4Request($result, 'result/responseStatus/status');
        $errorCode = $comviqHelper->getDataFromSS4Request($result, 'result/responseStatus/errorCode');
        $errorName = $comviqHelper->getDataFromSS4Request($result, 'result/responseStatus/errorName');
        if ($status == 'OK') {
            $session->addSuccess(
                $ss4IntegrationHelper->__(
                    'The Ss4 order %s has been canceled successfully.',
                    $ss4OrderId
                )
            );
        } elseif ($errorCode || $errorName) {
            $message = $ss4IntegrationHelper->__(
                'The Ss4 order %s has not been canceled. Error %s, %s',
                $ss4OrderId,
                $errorCode,
                $errorName
            );
            throw new Exception($message);
        } else {
            $message = $ss4IntegrationHelper->__(
                'The Ss4 did not respond or the response was incorrect. The Ss4 order %s has not been canceled',
                $ss4OrderId
            );
            throw new Exception($message);
        }
    }

    protected function _getWsdlType($order)
    {
        $wsdlType = 'ss4';
        if ($order->getShippingCarrier() instanceof Comviq_Togo_Model_Carrier_Togo) {
            $wsdlType = 'togo';
        }
        return $wsdlType;
    }

    public function updateTogoShipping($event)
    {
        $order = $event->getOrder();
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        if ($quote->getShippingAddress()->getShippingMethod() == 'togo_togo' && $order->getId() && $quote->getId()) {
            Mage::getModel('comviq_sales/shipping_togo')
                ->loadByQuoteId($quote->getId())
                ->setOrderId($order->getId())
                ->save();
        }
    }

    /**
     * Updates order grid
     *
     * @param Varien_Event_Observer $event
     */
    public function updateGrid($event)
    {
        /* @var $order Comviq_Sales_Model_Order */
        $order = $event->getOrder();
        $this->_setItemBind($order);
        $this->_setPaymentBind($order);

        $this->_updateGridByOrder($order->getId(), $this->_gridBind);
    }

    /**
     * Sets data bind to update order grid table
     *
     * @param array $bind
     */
    protected function _setGridBind($bind)
    {
        if (is_array($bind)) {
            if (!count($this->_gridBind)) {
                $this->_gridBind = $bind;
            } else {
                $this->_gridBind = array_merge($this->_gridBind, $bind);
            }
        }
    }

    /**
     * Sets item fields values for order grid
     *
     * @param Comviq_Sales_Model_Order $order
     * @return array
     */
    protected function _setItemBind($order)
    {
        $ownerItem = Mage::helper('comviq_checkout')
            ->getSubscriptionOwnerItemFromOrder($order);
        $bind = array(
            'subscription_name' => $ownerItem->getSubscriptionName(),
            'tel_number' => $ownerItem->getTelNumber(),
            'activation_type' => $ownerItem->getActivationType(),
            'previous_subscription_type' => $ownerItem->getPreviousSubscriptionType(),
            'previous_price_plan' => $ownerItem->getPreviousPricePlan(),
            'previous_discount_level' => $ownerItem->getPreviousDiscountLevel(),
            'is_validated_in_cbs' => $ownerItem->getIsValidatedInCbs(),
        );

        $this->_setGridBind($bind);
    }

    /**
     * Sets payment method name for the order in grid
     *
     * @param Comviq_Sales_Model_Order $order
     * @return array
     */
    protected function _setPaymentBind($order)
    {
        $bind = array(
            'payment_method_code' => $order->getPayment()->getMethodInstance()->getCode(),
        );

        $this->_setGridBind($bind);
    }

    /**
     * Executes SQL update on order_grid table
     *
     * @param $orderId int
     * @param $bind array
     * @throws Zend_Db_Adapter_Exception
     */
    protected function _updateGridByOrder($orderId, $bind)
    {
        $dbAdapter = $this->_getWriteAdapter();
        $table = $dbAdapter->getTableName('sales_flat_order_grid');
        $dbAdapter->update($table, $bind, 'entity_id = ' . $orderId);
    }

    /**
     * @return Varien_Db_Adapter_Pdo_Mysql
     */
    protected function _getWriteAdapter()
    {
        return Mage::getSingleton('core/resource')->getConnection('core_write');
    }
}
