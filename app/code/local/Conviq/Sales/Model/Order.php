<?php

class Comviq_Sales_Model_Order extends Mage_Sales_Model_Order
{
    protected $_offerModel = null;
    protected $_canSendNewEmailFlag = false;
    protected $_checkoutHelper;
    protected $_catalogHelper;

    protected $_downgradeCodes = array(
        'CREDIT_CONTROL_REJECTED',
        'CREDIT_CONTROL_REJECTED_LIMITED_APPROVAL_LEVEL_0_NOT_SUFFICIENT',
        'CREDIT_CONTROL_REJECTED_LIMITED_APPROVAL_LEVEL_1_NOT_SUFFICIENT'
    );

    protected $_recoverableErrors = array(
        'ERROR_CREATING_INVOICE',
        'ERROR_CREATING_INVOICE_ESTORE_OVERRUN',
    );

    protected function _setPaymentName()
    {
        $payment = $this->getPayment();
        $paymentMethod = $payment->getMethod();
        if ($paymentMethod == 'comviq_invoice' ||
            $paymentMethod == 'free' ||
            $this->getShippingMethod() == 'togo_togo'
        ) {
            $this->setPaymentName('');
        } else {
            $this->setPaymentName($payment->getMethodInstance()->getTitle());
        }
    }

    protected function _setShowShipping()
    {
        $this->setShowShipping(1);
        $noShippingName = Mage::getModel("comviq_shipping/carrier_noshipping")->getShippingMethodName();
        if (!$this->getIsNotVirtual() || $this->getShippingMethod() == $noShippingName) {
            $this->setShowShipping(0);
        }
    }

    protected function _setDeliveryData($subsOwnerItem)
    {
        $this->setExpectedDeliveryTime($subsOwnerItem->getData('expected_delivery_time'));

        $street = (array)$this->getShippingAddress()->getStreet();
        $this->setDeliveryAddressStreet(array_shift($street));
        $this->setDeliveryAddressName($this->getShippingAddress()->getName());
        $this->setDeliveryAddressPostcode($this->getShippingAddress()->getPostcode());
        $this->setDeliveryAddressCity($this->getShippingAddress()->getCity());
        $this->setExpectedDelivery($this->getExpectedDeliveryText());
    }

    /**
     * @param $subsOwnerItem
     */
    protected function _getShippingMethod($subsOwnerItem)
    {
        switch ($this->getShippingMethod()) {
            case 'togo_togo':
                $this->setIsToGo(true);
                $this->setShippingDescription(Mage::getStoreConfig('carriers/togo/name', $this->getStoreId()));
                $this->applyTogoStoreInfo();
                $this->setHasSpecificBody(true);

                if ($this->_hasDeviceAndNew()) {
                    $this->setIsToGoAndDeviceWithNew(true);
                } elseif ($subsOwnerItem->getActivationType() == 'PORT') {
                    $this->setIsToGoAndActivationTypePort(true);
                } else {
                    $this->setIsToGoAndNotDeviceWithNew(true);
                }
                break;
            case 'flatrate_flatrate':
                $this->setShippingDescription(Mage::getStoreConfig('carriers/flatrate/name', $this->getStoreId()));
                $item = Mage::helper('comviq_checkout')->getSubscriptionOwnerItemFromOrder($this);
                if ($item->getActivationType() == Comviq_Checkout_Model_Activationtype::ACTIVATION_TYPE_NEW
                    || $item->getActivationType() == Comviq_Checkout_Model_Activationtype::ACTIVATION_TYPE_PROLONG
                ) {
                    $this->setShippingDescription(Mage::getStoreConfig('carriers/flatrate/name', $this->getStoreId()));
                }
                break;
        }
    }

    /**
     * @param $isSend
     * @return bool|Comviq_Sales_Model_Order
     */
    public function isSendNeeded($isSend)
    {
        $result = false;
        if (!Mage::app()->getStore()->isAdmin()) {
            if (!$isSend) {
                $result = $this;
            }
        }

        return $result;
    }

    /**
     * Send email with order data
     * @param bool|false $isSend
     * @return Mage_Sales_Model_Order
     * @throws Comviq_Checkout_CheckoutException
     * @throws Exception
     */
    public function sendNewOrderEmail($isSend = false)
    {
        if (!$this->isSendNeeded($isSend)) {
            $this->setEmailSent(false);

            /* @var Comviq_Checkout_Helper_Data $checkoutHelper */
            $checkoutHelper = Mage::helper('comviq_checkout');
            $subsOwnerItem = $checkoutHelper->getSubscriptionOwnerItemFromOrder($this);

            if ($cPppPaymentText = $checkoutHelper->getCpppPaymentText($this)) {
                $this->setCpppPaymentText($cPppPaymentText);
            }

            $this->_setPaymentName();
            $this->_getShippingMethod($subsOwnerItem);

            $this->_setShowShipping();

            if (Mage::getStoreConfig('carriers/togo/enable_yellow_ball')) {
                $this->setTextYellowBall(Mage::getStoreConfig('carriers/togo/yellow_ball_text'));
            }
            if ($this->_has4Gb()) {
                $this->setIs4Gb(true);
                $this->setHasSpecificBody(true);
            }

            if (!$this->getIs4Gb() && !$this->getTextForVarvaCompaign()) {
                $this->setMailFirstRow(true);
            }

            $this->_setDeliveryData($subsOwnerItem);
            $isActivationTypeNew =
                $this->getActivationType() === Comviq_Checkout_Model_Activationtype::ACTIVATION_TYPE_NEW;
            $this->setIsActivationTypeNew($isActivationTypeNew);

            Mage::dispatchEvent('comviq_send_confirmation_mail_before', array('order' => $this));

            return parent::sendNewOrderEmail();
        }
    }
    
    public function applyTogoStoreInfo()
    {
        try {
            /** @var Comviq_Togo_Helper_Data $togoHelper */
            $togoHelper = Mage::helper("comviq_togo");
            /** @var Comviq_Sales_Model_Shipping_Togo $togoModel */
            $togoModel = Mage::getModel("comviq_sales/shipping_togo");
            $resellerTogo = $togoModel->load($this->getId(), 'order_id');
            $this->setToGoCity($resellerTogo->getData('city'));
            switch ($resellerTogo->getData('chain')) {
                case "PRESSBYRAN":
                    $chain = "Pressbyrån";
                    break;
                case "SEVEN_ELEVEN":
                    $chain = "7-Eleven";
                    break;
                default:
                    $chain = $resellerTogo->getData('chain');
                    break;
            }
            if ($resellerTogo->getData('chain_name')) {
                $chain = $resellerTogo->getData('chain_name');
            }
            $this->setToGoType($chain);
            $this->setToGoReseller($resellerTogo->getData('address'));
            $openToday = $togoHelper->getTogoShippingOpenToday($resellerTogo);
            $openingTime = $resellerTogo->getData('opening_time');
            $closingTime = $resellerTogo->getData('closing_time');
            $openingTimeSaturday = $resellerTogo->getData('opening_time_sat');
            $closingTimeSaturday = $resellerTogo->getData('closing_time_sat');
            $openingTimeSunday = $resellerTogo->getData('opening_time_sun');
            $closingTimeSunday = $resellerTogo->getData('closing_time_sun');
            $openClose = $togoHelper->formatHours($openingTime) . ' - ' . $togoHelper->formatHours($closingTime);
            $openCloseSaturday = $togoHelper->formatHours($openingTimeSaturday) . ' - ' .
                $togoHelper->formatHours($closingTimeSaturday);
            $openCloseSunday = $togoHelper->formatHours($openingTimeSunday) . ' - ' .
                $togoHelper->formatHours($closingTimeSunday);
            $this->setOpenToday($openToday);
            $this->setOpenClose($openClose);
            $this->setOpenCloseSaturday($openCloseSaturday);
            $this->setOpenCloseSunday($openCloseSunday);
            $url = '@' . round($resellerTogo->getData('latitude'), 7) . ',' .
                round($resellerTogo->getData('longitude'), 7);
            $googleMapsUrl = '//maps.google.com/maps?q=' . rawurlencode($url) . '&iwloc=near&t=m&z=15';
            $this->setGoogleMapUrl($googleMapsUrl);
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }
    
    public function getSimpleFromItem($_item)
    {
        if ($_item->getProductType() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE) {
            $childrenItems = $_item->getChildrenItems();
            if (count($childrenItems)) {
                return array_shift($childrenItems);
            }
        }
        return $_item;
    }

    protected function _has4Gb()
    {
        $items = $this->getAllVisibleItems();
        if (count($items)) {
            foreach ($items as $item) {
                if ($item->getProduct()->getFourGb()) {
                    return true;
                }
            }
        }
        return false;
    }

    protected function _hasDeviceAndNew()
    {
        /* @var $catalogHelper Comviq_Catalog_Helper_Data */
        $catalogHelper = Mage::helper('comviq_catalog');
        foreach ($this->getAllItems() as $_item) {
            if (
                $catalogHelper->isMobile($_item->getProduct()->getAttributeSetId()) &&
                $_item->getActivationType() == 'NEW'
            ) {
                return true;
            }
        }
        return false;
    }

    /**
     * Correcting date, using Timezone (from Locale Options)
     * magento
     *
     * @return bool|string
     */
    public function getCreatedAt($correctByLocale = false)
    {
        $createdAt = $this->getData('created_at');
        if ($correctByLocale && !is_null($createdAt)) {
            $createdAt = Mage::helper('core')->formatDate($createdAt, 'medium', false);
        }
        return $createdAt;
    }

    public function getExpectedDeliveryText()
    {
        return Mage::helper('comviq_sales')->getExpectedDeliveryText($this);
    }

    /**
     * Returns the orders's payment method for SS4
     *
     * @return string
     */
    public function getSs4PaymentMethod()
    {
        /*"BANK_GIRO" in all cases except KONTROLL subscription
        "BANK_GIRO" in case of KONTROLL + payment:faktura
        "CREDITCARD" in case of KONTROLL + payment:DIBS*/
        $method = 'PLUS_GIRO';
        if ($this->_isControl() && $this->getPayment()->getMethod() == 'comviq_dibsfw') {
            $method = 'CREDIT_CARD';
        }
        return $method;
    }

    /**
     * Returns the customer's subtype for SS4
     *
     * @return string
     */
    public function getCustomerSubType()
    {
        /*"NORMAL" in all cases except KONTROLL subscription
        "CREDITCARDNOINKASSO" in case of KONTROLL + payment:DIBS
        "CREDITCARDNOINKASSO" in case of KONTROLL + payment:faktura*/
        $type = 'RESIDENTIAL';
        if ($this->_isControl()) {
            $type = 'CREDIT_CARD_LIMITED';

            $quote = Mage::getSingleton('checkout/session')->getQuote();
            $paymentInstance = $quote->getPayment()->getMethodInstance();
            if ($paymentInstance instanceof Comviq_Payment_Model_Method_Faktura) {
                $type = 'RESIDENTIAL_GO';
            }
        }
        return $type;
    }

    /**
     * Check if the order has a control subscription
     *
     * @return bool
     */
    public function isControl()
    {
        return $this->_isControl();
    }

    /**
     * Check if the order is for Control subscription
     *
     * @return bool
     */
    protected function _isControl()
    {
        return $this->_getCheckoutHelper()
            ->getSubscriptionOwnerItemFromOrder($this)
            ->getSubscriptionIsControl();
    }

    /**
     * Check if the order is for Control with SSN
     *
     * @return bool
     */
    public function isControlWithoutSsn()
    {
        if ($this->_isControl()) {
            /* @var $checkoutHelper Comviq_Checkout_Helper_Data */
            $checkoutHelper = Mage::helper('comviq_checkout');
            $aTypeFromConfig = Mage::getStoreConfig('comviq/kontroll/activation_types');
            $aTypesForControl = explode(',', $aTypeFromConfig);
            $subsOwnerItem = $checkoutHelper->getSubscriptionOwnerItemFromOrder($this);
            $activationType = $subsOwnerItem->getData('activation_type');
            if (!in_array($activationType, $aTypesForControl)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns the orders's invoice media for SS4
     * For all cases hardcoded for now, since we don't provide logic for choosing of invoice type
     *
     * @return string
     */
    public function getInvoiceMedia()
    {
        return 'EMAIL';
    }

    /**
     * Returns CC details for the payment
     *
     * @return string
     */
    public function getCreditCardDetails()
    {
        $details = null;
        try {
            $payment = $this->getPayment();
            if (
                $payment->getCcNumberEnc() && $payment->getCcType()
                &&
                $payment->getCcExpYear() && $payment->getCcExpMonth()
            ) {
                $details['maskedCreditCardNumber'] = $payment->getCcNumberEnc();
                $expDate = str_pad($payment->getCcExpMonth(), 2, '0', STR_PAD_LEFT) . $payment->getCcExpYear();
                $details['expiryDate'] = $expDate;
                $details['type'] = $payment->getCcType();
            }
        } catch (Exception $e) {
            Mage::logException($e);
        }

        return $details;
    }

    public function getPartPaymentOption()
    {
        $result = false;
        $selectedOptionData = $this->getPayment()->getAdditionalInformation('selected_option');

        if (is_array($selectedOptionData) && count($selectedOptionData)) {
            $result = $selectedOptionData;
        }

        return $result;
    }

    /**
     * Checks if the order is downgraded by Super Store
     *
     * @return bool
     */
    public function isDowngrade()
    {
        $isEnabled = Mage::getStoreConfig('comviq/downgrade/downgrade_enabled');
        if ($isEnabled) {
            if (Mage::getStoreConfig('comviq/downgrade/downgrade_error')) {
                return true;
            }
            if (in_array($this->getSuperStoreStatus(), $this->_downgradeCodes)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns true if the Order is a successful in SS4
     *
     * @return bool
     */
    public function isSs4Successful()
    {
        $superStoreId = $this->getData('super_store_id');
        if (empty($superStoreId)) {
            return false;
        } else {
            Mage::getSingleton('customer/session')->setSuccessOrderId($this->getId());
            return true;
        }
    }

    public function isRecoverable()
    {
        if ($this->getSuperStoreErrorName() && in_array($this->getSuperStoreErrorName(), $this->_recoverableErrors)) {
            $this->_getCheckoutHelper()->getCheckout()->setData('klarna_fail', 1);

            return true;
        }

        return false;
    }

    /**
     * Returns Success Order Ids By SSN and Subsidy Id
     * @param $ssn string
     * @param $subsidyId int
     *
     * @return mixed
     */
    public function getSuccessOrderIdsBySsnAndSubsidy($ssn, $subsidyId)
    {
        return $this->getResource()
            ->getSuccessOrderIdsBySsnAndSubsidy($ssn, $subsidyId);
    }

    /**
     * Returns Success Order Ids By Child's SSN and Subsidy Id
     * @param $ssn string
     * @param $subsidyId int
     *
     * @return mixed
     */
    public function getSuccessOrderIdsByChildSsnAndSubsidy($ssn, $subsidyId)
    {
        return $this->getResource()
            ->getSuccessOrderIdsByChildSsnAndSubsidy($ssn, $subsidyId);
    }

    /**
     * Get formated order created date in store timezone
     *
     * @param   string $format date format type (short|medium|long|full)
     * @param   boolean $displayTime
     * @return  string
     */
    public function getCreatedAtFormated($format, $displayTime = false)
    {
        return Mage::helper('core')->formatDate($this->getCreatedAtStoreDate(), $format, $displayTime);
    }

    public function getTogoOpenningTimeByOrder($orderId = false)
    {
        if (!$orderId) {
            $orderId = $this->getId();
        }

        $shippingTogo = Mage::getModel('comviq_sales/shipping_togo')->load($orderId, 'order_id');
        $openTime = '';
        if ($shippingTogo->getData('opening_time') && $shippingTogo->getData('closing_time')) {
            $openTime .= "Mån - Fre "
                .Mage::helper('comviq_togo')->formatHours($shippingTogo->getData('opening_time')) ." - "
                .Mage::helper('comviq_togo')->formatHours($shippingTogo->getData('closing_time'));
        }
        if ($shippingTogo->getData('opening_time_sat') && $shippingTogo->getData('closing_time_sat')) {
            $openTime .= "<br />\nLör "
                .Mage::helper('comviq_togo')->formatHours($shippingTogo->getData('opening_time_sat')) ." - "
                .Mage::helper('comviq_togo')->formatHours($shippingTogo->getData('closing_time_sat'));
        }
        if ($shippingTogo->getData('opening_time_sun') && $shippingTogo->getData('closing_time_sun')) {
            $openTime .= "<br />\nSön "
                .Mage::helper('comviq_togo')->formatHours($shippingTogo->getData('opening_time_sun')) ." - "
                .Mage::helper('comviq_togo')->formatHours($shippingTogo->getData('closing_time_sun'));
        }
        return $openTime;
    }

    /**
     * get Accessory Product from order
     *
     * @return Mage_Sales_Model_Order_Item $item
     */
    public function getAccessoryProduct()
    {
        /** @var $catalogHelper Comviq_Catalog_Helper_Data */
        $catalogHelper = Mage::helper('comviq_catalog');
        $items = $this->getItemsCollection();
        foreach ($items as $item) {
            if ($catalogHelper->isAccessory($item->getProduct())) {
                return $item;
            }
        }

        return null;
    }

    /**
     * Returns ActivationType of subscription owner item's
     *
     * @return mixed
     */
    public function getActivationType()
    {
        return $this->_getCheckoutHelper()
            ->getSubscriptionOwnerItemFromOrder($this)
            ->getData('activation_type');
    }

    public function isCuseOrder()
    {
        return $this->getCuseAgentId() ? true : false;
    }

    /**
     * @return Comviq_Checkout_Helper_Data
     */
    protected function _getCheckoutHelper()
    {
        if (!$this->_checkoutHelper) {
            $this->_checkoutHelper = Mage::helper('comviq_checkout');
        }
        return $this->_checkoutHelper;
    }

    /**
     * @return Comviq_Catalog_Helper_Data
     */
    protected function _getCatalogHelper()
    {
        if (!$this->_catalogHelper) {
            $this->_catalogHelper = Mage::helper('comviq_catalog');
        }
        return $this->_catalogHelper;
    }

    /**
     * Returns true if the order item is simonly
     *
     * @return bool
     * @throws Comviq_Checkout_CheckoutException
     */
    public function isSimonlyOrder()
    {
        $subsOwnerItem = $this->_getCheckoutHelper()->getSubscriptionOwnerItemFromOrder($this);
        return $this->_getCatalogHelper()->isSimonly($subsOwnerItem->getProduct());
    }

    /**
     * @return Mage_Sales_Model_Order_Item
     * @throws Comviq_Checkout_CheckoutException
     */
    public function getSubscriptionOwnerItemFromOrder()
    {
        return $this->_getCheckoutHelper()->getSubscriptionOwnerItemFromOrder($this);
    }

    public function getInsuranceRule()
    {
        $insuranceRule = Mage::getModel('comviq_insurance/rule')->load($this->getInsuranceRuleId());
        if ($insuranceRule->getId()) {
            return $insuranceRule;
        }
        return false;
    }
}
