<?php
class Comviq_Sales_Model_Quote extends Mage_Sales_Model_Quote
{
    public function setDataSaveDeny()
    {
        $this->_dataSaveAllowed = false;
    }

    //
    public function getItemByProduct($product)
    {
        return false;
    }

    /**
     * Save ToGo Shipping reseller details
     *
     * @param $session  Mage_Checkout_Model_Session
     * @param $request  Zend_Controller_Request_Abstract
     *
     * @return array
     */
    public function saveTogoShipping($session, $request)
    {
        $resellersToGo = $session->getResellersToGoInfo();
        if (
            ($reseller = $request->getParam('resellerToGo')) &&
            is_array($resellersToGo) && count($resellersToGo) &&
            $this->getId()
        ) {
            foreach ($resellersToGo as $resellerToGo) {
                if ($resellerToGo->distributingResellerNumber == $reseller) {
                    $shippingTogo = Mage::getModel('comviq_sales/shipping_togo');
                    try {
                        $shippingTogo->loadByQuoteId($this->getId());
                        $resellerData = array(
                            'id'              => $shippingTogo->getId(),
                            'quote_id'        => $this->getId(),
                            'distributing_reseller_number' => $reseller,
                            'city'             => $resellerToGo->city,
                            'area'             => (isset($resellerToGo->area) ? $resellerToGo->area : ''),
                            'longitude'        => (isset($resellerToGo->longitude) ? $resellerToGo->longitude : ''),
                            'latitude'         => (isset($resellerToGo->latitude) ? $resellerToGo->latitude : ''),
                            'address'          => (isset($resellerToGo->address) ? $resellerToGo->address : ''),
                            'opening_time'     => (isset($resellerToGo->openingTime) ? $resellerToGo->openingTime : ''),
                            'closing_time'     => (isset($resellerToGo->closingTime) ? $resellerToGo->closingTime : ''),
                            'opening_time_sat' => (isset($resellerToGo->openingTimeSaturday)
                                ? $resellerToGo->openingTimeSaturday : ''),
                            'closing_time_sat' => (isset($resellerToGo->closingTimeSaturday)
                                ? $resellerToGo->closingTimeSaturday : ''),
                            'opening_time_sun' => (isset($resellerToGo->openingTimeSunday)
                                ? $resellerToGo->openingTimeSunday : ''),
                            'closing_time_sun' => (isset($resellerToGo->closingTimeSunday)
                                ? $resellerToGo->closingTimeSunday : ''),
                            'chain'            => (isset($resellerToGo->chain)
                                ? $resellerToGo->chain : ''),
                            'chain_name'       => (isset($resellerToGo->chainName)
                                ? $resellerToGo->chainName : ''),
                        );
                        $shippingTogo = Mage::getModel('comviq_sales/shipping_togo')
                            ->addData($resellerData);
                        $shippingTogo->save();
                        return array();
                    } catch (Exception $e) {
                        Mage::logException($e);
                    }
                }
            }
        }
        Mage::helper('comviq_log')->log(
            Comviq_Log_Helper_Data::CHECKOUT_MODULE,
            Comviq_Log_Helper_Data::ORDER,
            Comviq_Log_Helper_Data::STATUS_OTHER,
            "saveTogoShipping:\n " . print_r(array(
                'paramResellerToGo' => $request->getParam('resellerToGo'),
                'resellersToGo' => print_r($resellersToGo, 1),
                'quoteId' => $this->getId()
            ), 1)
        );

        return array('error' => -1, 'message' => Mage::helper('checkout')->__('ToGo Invalid data.'));
    }
}
