<?php
/** @var $this Mage_Sales_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();

/** @var $installer Mage_Sales_Model_Resource_Setup */
$installer->addAttribute(
    'quote',
    'customer_agreement_by',
    array(
        'type' => 'varchar',
        'after' => 'assistant_data',
    )
);
$installer->addAttribute(
    'order',
    'customer_agreement_by',
    array(
        'type' => 'varchar',
        'after' => 'assistant_data',
    )
);

$installer->addAttribute(
    'quote',
    'customer_agreement_status',
    array(
        'type' => 'varchar',
        'after' => 'customer_agreement_by',
    )
);
$installer->addAttribute(
    'order',
    'customer_agreement_status',
    array(
        'type' => 'varchar',
        'after' => 'customer_agreement_by',
    )
);

$installer->endSetup();