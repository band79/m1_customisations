<?php
/** @var $this Mage_Sales_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();

$installer->addAttribute(
    'order_item',
    'togo',
    array(
        'user_defined'               => true,
        'type'                       => 'int',
        'source'                     => 'eav/entity_attribute_source_boolean',
        'label'                      => 'Togo',
        'required'                   => true,
        'input'                      => 'boolean',
        'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'default'                    => 0,
    )
);

$installer->endSetup();
