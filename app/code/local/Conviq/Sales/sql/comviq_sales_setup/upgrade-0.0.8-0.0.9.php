<?php
/** @var $this Mage_Sales_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();

/** @var Mage_Sales_Model_Resource_Setup Mage_Sales_Model_Resource_Setup */

$quoteTable = $installer->getTable('sales/quote_item');
$orderTable = $installer->getTable('sales/order_item');
$connection = $installer->getConnection();

$installer->run("ALTER TABLE {$quoteTable} ADD COLUMN `subscription_name` varchar(80) NULL DEFAULT NULL COMMENT 'Subscription Name';");
$installer->run("ALTER TABLE {$orderTable} ADD `subscription_name` varchar(80) NULL DEFAULT NULL COMMENT 'Subscription Name';");

$installer->endSetup();
