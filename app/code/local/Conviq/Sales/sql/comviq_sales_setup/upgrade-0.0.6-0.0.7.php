<?php
/** @var $this Mage_Sales_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();

/** @var Mage_Sales_Model_Resource_Setup Mage_Sales_Model_Resource_Setup */

$quoteTable = $installer->getTable('sales/quote_item');
$orderTable = $installer->getTable('sales/order_item');
$connection = $installer->getConnection();

$attributes = array (
    'subscription_id' => array(
        'attribute' => array('type' => 'bigint', 'comment' => 'Subscription ID'),
        'db' => array('type' => 'int(11)', 'comment' => 'Subscription ID')
    ),
    'articleid' => array(
        'attribute' => array('type' => 'varchar', 'size' => '40', 'comment' => 'Binding Period Article ID'),
        'db' => array('type' => 'varchar(40)', 'comment' => 'Binding Period Article ID')
    ),
    'billplan' => array(
        'attribute' => array('type' => 'varchar', 'size' => '6', 'comment' => 'Plan Code'),
        'db' => array('type' => 'varchar(6)', 'comment' => 'Plan Code')
    ),
    'monthly_price' => array(
        'attribute' => array('type' => 'decimal', 'comment' => 'Monthly price'),
        'db' => array('type' => 'decimal', 'comment' => 'Monthly price')
    ),
    'bp' => array(
        'attribute' => array('type' => 'int', 'comment' => 'Binding Period'),
        'db' => array('type' => 'int(11)', 'comment' => 'Binding Period')
    ),
    'subscription_type' => array(
        'attribute' => array('type' => 'varchar', 'size' => '40', 'comment' => 'Subscription Type'),
        'db' => array('type' => 'varchar(40)', 'comment' => 'Subscription Type')
    ),
    'activation_type' => array(
        'attribute' => array('type' => 'varchar', 'size' => '12', 'comment' => 'Activation Type'),
        'db' => array('type' => 'varchar(12)', 'comment' => 'Activation Type')
    ),
    'tel_number' => array(
        'attribute' => array('type' => 'varchar', 'size' => '12', 'comment' => 'Phone Number'),
        'db' => array('type' => 'varchar(12)', 'comment' => 'Phone Number')
    ),
    'subscription_configs_data' => array(
        'attribute' => array('type' => 'text', 'comment' => 'Subscription Config Data'),
        'db' => array('type' => 'text', 'comment' => 'Subscription Config Data, serialized')
    )
);

foreach ($attributes as $attributeCode => $attribute) {
    $childColumnParam = "{$attribute['db']['type']} NULL DEFAULT NULL COMMENT '{$attribute['db']['comment']}';";
    if (!$connection->tableColumnExists($quoteTable, $attributeCode)) {
        $installer->addAttribute('quote_item', $attributeCode, $attribute['attribute']);
        if (!$connection->tableColumnExists($quoteTable, $attributeCode)) {
            $installer->run(
                "ALTER TABLE {$quoteTable} ADD COLUMN `".$attributeCode."` " . $childColumnParam
            );
        }
    }
    if (!$connection->tableColumnExists($orderTable, $attributeCode)) {
        $installer->addAttribute('order_item', $attributeCode, $attribute['attribute']);
        if (!$connection->tableColumnExists($orderTable, $attributeCode)) {
            $installer->run(
                "ALTER TABLE {$orderTable} ADD COLUMN `".$attributeCode."` " . $childColumnParam
            );
        }
    }
}

$installer->endSetup();
