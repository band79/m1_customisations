<?php
/** @var $this Mage_Sales_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();

$installer->addAttribute('quote_item', 'billing_id', array(
    'type'  => Varien_Db_Ddl_Table::TYPE_VARCHAR,
    'after' => 'product_article_id',
));
$installer->addAttribute('order_item', 'billing_id', array(
    'type'  => Varien_Db_Ddl_Table::TYPE_VARCHAR,
    'after' => 'product_article_id',
));

$installer->endSetup();
