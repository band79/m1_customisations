<?php
/** @var $this Mage_Sales_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();

/** @var Mage_Sales_Model_Resource_Setup Mage_Sales_Model_Resource_Setup */

$installer->addAttribute(
    'quote_item',
    'is_validated_in_cbs',
    array(
        'type'      => 'varchar',
        'size'      => '40',
        'comment'   => 'Is Validated in CBS',
        'after'     => 'previous_price_plan'
    )
);

$installer->addAttribute(
    'order_item',
    'is_validated_in_cbs',
    array(
        'type'      => 'varchar',
        'size'      => '40',
        'comment'   => 'Is Validated in CBS',
        'after'     => 'previous_price_plan'
    )
);

$installer->endSetup();
