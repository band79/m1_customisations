<?php
/** @var $this Mage_Sales_Model_Resource_Setup */
$logName = 'order_grid_items_update.log';
Mage::log('start update', null, $logName);

$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$gridTable = $installer->getTable('sales/order_grid');
$orderItemTable = $installer->getTable('sales/order_item');
$fields = $connection->describeTable($gridTable);
$fieldNames = array_keys($fields);
$after = array_pop($fieldNames);

$columns = array(
    'subscription_name',
    'tel_number',
    'activation_type',
    'previous_subscription_type',
    'previous_price_plan',
    'previous_discount_level',
    'is_validated_in_cbs',
);
$columnParams = array (
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'nullable'  => true,
    'length' => 255,
    'comment'   => 'Payment Method Code',
    'after'   => 'shipping_method',
);

Mage::log('before adding fields', null, $logName);

foreach ($columns as $columnName) {
    if (!$connection->tableColumnExists($gridTable, $columnName)) {
        try {
            $columnParams['after'] = $after;
            $connection->addColumn($gridTable, $columnName, $columnParams);
            $after = $columnName;
            Mage::log('ADDED ' . $columnName, null, $logName);
        } catch (Exception $e) {
            Mage::log($columnName . ' ' . $e->getCode() . ' ' . $e->getMessage(), null, $logName);
        }
    }
}
Mage::log('after adding fields', null, $logName);

$select = $connection->select()->from(array('grid_table' => $gridTable), $columns);
$select->reset()->joinLeft(
    array('order_item_table' => $orderItemTable),
    'order_item_table.product_type = "simple" AND grid_table.entity_id = order_item_table.order_id',
    $columns
);
$updateSql = $select->crossUpdateFromSelect(array('grid_table' => $gridTable));
$connection->beginTransaction();
try {
    $connection->query($updateSql);
    $connection->commit();
} catch (Exception $e) {
    $connection->rollback();
    Mage::log('ERROR during update order grid table. ' . $e->getCode() . ' ' . $e->getMessage(), null, $logName);
}

Mage::log('finish update', null, $logName);

$installer->endSetup();
