<?php
/** @var $this Mage_Sales_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();

/** @var Mage_Sales_Model_Resource_Setup Mage_Sales_Model_Resource_Setup */

$installer->addAttribute(
    'quote_item',
    'previous_price_plan',
    array(
        'type'      => 'varchar',
        'size'      => '40',
        'comment'   => 'Previous Price Plan',
        'after'     => 'previous_subscription_type'
    )
);
$installer->addAttribute(
    'quote_item',
    'previous_discount_level',
    array(
        'type'      => 'varchar',
        'size'      => '40',
        'comment'   => 'Previous Discount Level',
        'after'     => 'previous_subscription_type'
    )
);

$installer->addAttribute(
    'order_item',
    'previous_price_plan',
    array(
        'type'      => 'varchar',
        'size'      => '40',
        'comment'   => 'Previous Price Plan',
        'after'     => 'previous_subscription_type'
    )
);
$installer->addAttribute(
    'order_item',
    'previous_discount_level',
    array(
        'type'      => 'varchar',
        'size'      => '40',
        'comment'   => 'Previous Discount Level',
        'after'     => 'previous_subscription_type'
    )
);

$installer->endSetup();
