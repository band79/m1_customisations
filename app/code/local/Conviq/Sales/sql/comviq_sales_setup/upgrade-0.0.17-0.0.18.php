<?php
/** @var $this Mage_Sales_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();

$installer->addAttribute(
    'quote_item',
    'subscription_group_id',
    array(
        'type'    => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'after'   => 'subscription_id',
        'comment' => 'Subscription Group ID',
    )
);
$installer->addAttribute(
    'quote_item',
    'is_togo',
    array(
        'type'    => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'after'   => 'subscription_group_id',
        'comment' => 'Is ToGo Available',
    )
);

$installer->addAttribute(
    'order_item',
    'subscription_group_id',
    array(
        'type'    => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'after'   => 'subscription_id',
        'comment' => 'Subscription Group ID',
    )
);
$installer->addAttribute(
    'order_item',
    'is_togo',
    array(
        'type'    => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        'after'   => 'subscription_group_id',
        'comment' => 'Is ToGo Available',
    )
);
$installer->endSetup();
