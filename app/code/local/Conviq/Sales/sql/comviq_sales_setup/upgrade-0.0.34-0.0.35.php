<?php
/** @var $this Mage_Sales_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$installer->addAttribute('quote', 'is_downgrade', array('type' => 'smallint'));
$installer->addAttribute('order', 'is_downgrade', array('type' => 'smallint', 'grid' => true));

$installer->endSetup();
