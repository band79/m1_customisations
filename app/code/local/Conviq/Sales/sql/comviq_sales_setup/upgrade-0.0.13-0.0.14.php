<?php
/** @var $this Mage_Sales_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();

$attrText = array(
    'type' => Varien_Db_Ddl_Table::TYPE_VARCHAR
);

$installer->addAttribute('order', 'super_store_id', $attrText);
$installer->addAttribute('order', 'super_store_status', $attrText);

$orderTable = $installer->getTable('sales/order');
$connection = $installer->getConnection();
$connection->addIndex(
    $orderTable,
    $installer->getIdxName($orderTable, array('super_store_id')),
    array('super_store_id')
);

$installer->endSetup();
