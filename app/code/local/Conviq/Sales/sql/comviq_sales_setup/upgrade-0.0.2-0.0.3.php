<?php

$installer = $this;
$installer->startSetup();

/** @var Mage_Sales_Model_Resource_Setup Mage_Sales_Model_Resource_Setup */
$installer->addAttribute('quote', 'reseller_id', array('type' => 'varchar'));
$installer->addAttribute('order', 'reseller_id', array('type' => 'varchar'));

$installer->endSetup();
