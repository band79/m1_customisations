<?php
/** @var $this Mage_Sales_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();

$attrBoolean = array(
    'type' => Varien_Db_Ddl_Table::TYPE_BOOLEAN
);
$attrText = array(
    'type' => Varien_Db_Ddl_Table::TYPE_VARCHAR
);

$installer->addAttribute('quote_item', 'sim_type', $attrText);
$installer->addAttribute('order_item', 'sim_type', $attrText);

$installer->addAttribute('quote_item', 'sim_attached', $attrBoolean);
$installer->addAttribute('order_item', 'sim_attached', $attrBoolean);

$installer->endSetup();
