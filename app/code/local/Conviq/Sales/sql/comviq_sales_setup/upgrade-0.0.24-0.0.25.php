<?php
/** @var $this Mage_Sales_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();

$installer->addAttribute(
    'order',
    'presmoker',
    array(
        'type'    => Varien_Db_Ddl_Table::TYPE_SMALLINT,
        'after'   => 'subscriptions',
        'comment' => 'Does it presmoker order',
    )
);
$installer->endSetup();
