<?php
/** @var $this Mage_Sales_Model_Resource_Setup */
$logName = 'order_grid_data_update.log';
Mage::log('start update', null, $logName);

$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$gridTable = $installer->getTable('sales/order_grid');
$orderTable = $installer->getTable('sales/order');
$collumns = array(
    'child_ssn',
    'coupon_code',
    'sku',
    'ssn',
    'devices',
    'subscriptions',
    'reseller_id',
    'cuse_agent_id',
    'installment_period',
    'installment_monthly_price',
    'phone_notification',
    'customer_email',
    'shipping_documents',
    'shipping_method'
);
$select = $connection->select()->from(array('grid_table' => $gridTable), $collumns);
$select->reset()->joinLeft(array('order_table' => $orderTable), 'grid_table.entity_id = order_table.entity_id', $collumns);
$updateSql = $select->crossUpdateFromSelect(array('grid_table' => $gridTable));

$connection->beginTransaction();
try {
    $connection->query($updateSql);
    $connection->commit();
} catch (Exception $e) {
    $connection->rollback();
    Mage::log('ERROR during update order grid table. ' . $e->getCode() . ' ' . $e->getMessage(), null, $logName);
}

Mage::log('finish update', null, $logName);

$installer->endSetup();
