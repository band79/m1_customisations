<?php
/** @var $this Mage_Sales_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();

/** @var Mage_Sales_Model_Resource_Setup Mage_Sales_Model_Resource_Setup */

$quoteTable = $installer->getTable('sales/quote_item');
$orderTable = $installer->getTable('sales/order_item');
$connection = $installer->getConnection();

$installer->run("ALTER TABLE {$quoteTable} ADD COLUMN `expected_delivery_time` varchar(80) NULL DEFAULT NULL COMMENT 'Expected Delivery Time';");
$installer->run("ALTER TABLE {$orderTable} ADD `expected_delivery_time` varchar(80) NULL DEFAULT NULL COMMENT 'Expected Delivery Time';");

$installer->run("ALTER TABLE {$quoteTable} ADD COLUMN `product_make` varchar(32) NULL DEFAULT NULL COMMENT 'Product Make';");
$installer->run("ALTER TABLE {$orderTable} ADD `product_make` varchar(32) NULL DEFAULT NULL COMMENT 'Product Make';");

$installer->run("ALTER TABLE {$quoteTable} ADD COLUMN `product_partner_id` varchar(12) NULL DEFAULT NULL COMMENT 'Product Partner Id';");
$installer->run("ALTER TABLE {$orderTable} ADD `product_partner_id` varchar(12) NULL DEFAULT NULL COMMENT 'Product Partner Id';");

$installer->run("ALTER TABLE {$quoteTable} ADD COLUMN `product_article_id` varchar(12) NULL DEFAULT NULL COMMENT 'Product Article Id';");
$installer->run("ALTER TABLE {$orderTable} ADD `product_article_id` varchar(12) NULL DEFAULT NULL COMMENT 'Product Article Id';");

$installer->endSetup();
