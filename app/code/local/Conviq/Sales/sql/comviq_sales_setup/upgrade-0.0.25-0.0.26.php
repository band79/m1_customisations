<?php
/** @var $this Mage_Sales_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();

/** @var Mage_Sales_Model_Resource_Setup Mage_Sales_Model_Resource_Setup */

$installer->addAttribute(
    'quote_item', 
    'previous_subscription_type', 
    array(
        'type'      => 'varchar',
        'size'      => '40',
        'comment'   => 'Previous Subscription Type',
        'after'     => 'subscription_type'
    )
);

$installer->addAttribute(
    'order_item',
    'previous_subscription_type',
    array(
        'type'      => 'varchar',
        'size'      => '40',
        'comment'   => 'Previous Subscription Type',
        'after'     => 'subscription_type'
    )
);

$installer->endSetup();
