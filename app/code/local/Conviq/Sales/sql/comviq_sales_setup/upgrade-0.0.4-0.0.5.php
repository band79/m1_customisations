<?php
/** @var $this Mage_Sales_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();

$connection = $installer->getConnection();
$orderTable = $installer->getTable('sales/order');
$quoteTable = $installer->getTable('sales/quote');

if (!$connection->tableColumnExists($quoteTable, 'child_ssn')) {
    $installer->addAttribute('quote', 'child_ssn', array('type' => 'bigint', 'after'=>'student_data'));
}

if (!$connection->tableColumnExists($orderTable, 'child_ssn')) {
    $installer->addAttribute('order', 'child_ssn', array('type' => 'bigint', 'after'=>'student_data'));
}

$select = $connection->select()
    ->from($orderTable, array('entity_id', 'student_data'))
    ->where('student_data is not null')
    ->where('child_ssn is null');

$rows = $select->query()->fetchAll();
foreach ($rows as $row) {
    $childData = unserialize($row['student_data']);
    $connection->update(
        $orderTable,
        array('child_ssn' => $childData['ssn']),
        array('entity_id =?' => $row['entity_id'])
    );
}

$childColumnParam = "VARCHAR(255) NULL DEFAULT NULL COMMENT 'Data of child as customer';";
if (!$connection->tableColumnExists($quoteTable, 'child_data')) {
    $installer->run(
        "ALTER TABLE {$quoteTable} CHANGE COLUMN `student_data` `child_data` " . $childColumnParam
    );
}
if (!$connection->tableColumnExists($orderTable, 'child_data')) {
    $installer->run(
        "ALTER TABLE {$orderTable} CHANGE COLUMN `student_data` `child_data` " . $childColumnParam
    );
}

$installer->endSetup();