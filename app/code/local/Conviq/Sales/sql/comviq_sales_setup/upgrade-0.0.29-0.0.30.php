<?php
/** @var $this Mage_Sales_Model_Resource_Setup */
$logName = 'order_grid_update.log';
Mage::log('start', null, $logName);

$installer = $this;
$installer->startSetup();

/** @var Mage_Sales_Model_Resource_Setup Mage_Sales_Model_Resource_Setup */

Mage::log('definitions', null, $logName);
$connection = $installer->getConnection();
$gridTable = $installer->getTable('sales/order_grid');
$orderTable = $installer->getTable('sales/order');
$fields = $connection->describeTable($gridTable);
$fieldNames = array_keys($fields);
$after = array_pop($fieldNames);

$columns = array(
    'coupon_code' => array (
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => true,
        'length' => 255,
        'comment'   => 'Magento Native Coupone Code field'
    ),
    'ssn' => array (
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => true,
        'comment'   => 'Ssn'
    ),
    'child_ssn' => array (
        'type'      => Varien_Db_Ddl_Table::TYPE_BIGINT,
        'nullable'  => true,
        'comment'   => 'Child Ssn'
    ),
    'customer_email' => array (
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => true,
        'length' => 255,
        'comment'   => 'Customer Email'
    ),
    'sku' => array (
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => true,
        'length' => 255,
        'comment'   => 'SKU'
    ),
    'phone_notification' => array (
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => true,
        'comment'   => 'Mobile Phone'
    ),
    'devices' => array (
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => true,
        'comment'   => 'Device names'
    ),
    'subscriptions' => array (
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => true,
        'comment'   => 'Subscription types'
    ),
    'reseller_id' => array (
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => true,
        'length' => 255,
        'comment'   => 'Reseller Id'
    ),
    'cuse_agent_id' => array (
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => true,
        'length' => 255,
        'comment'   => 'Cuse Agent Id'
    ),
    'installment_period' => array (
        'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable'  => true,
        'unsigned'  => true,
        'length' => 10,
        'comment'   => 'Installment Period'
    ),
    'installment_monthly_price' => array (
        'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable'  => true,
        'unsigned'  => true,
        'length' => 10,
        'comment'   => 'Installment Period'
    ),
    'shipping_method' => array (
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => true,
        'length' => 255,
        'comment'   => 'Shipping Method'
    ),
    'shipping_documents' => array (
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => true,
        'length' => 255,
        'comment'   => 'Shipping Documents'
    ),
);

Mage::log('before adding fields', null, $logName);

foreach ($columns as $columnName => $columnParams) {
    if (!$connection->tableColumnExists($gridTable, $columnName)) {
        try {
            $columnParams['after'] = $after;
            $connection->addColumn($gridTable, $columnName, $columnParams);
            $after = $columnName;
            Mage::log('ADDED ' . $columnName, null, 'order_grid_update.log');
        } catch (Exception $e) {
            Mage::log($columnName . ' ' . $e->getCode() . ' ' . $e->getMessage(), null, $logName);
        }
    }
}
Mage::log('after adding fields', null, $logName);

$installer->endSetup();
