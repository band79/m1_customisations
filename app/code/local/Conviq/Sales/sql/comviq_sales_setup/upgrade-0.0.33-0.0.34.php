<?php
/** @var $this Mage_Sales_Model_Resource_Setup */
$logName = 'order_grid_items_update.log';
Mage::log('start update', null, $logName);

$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();

$gridQuoteOrderTables[] = $installer->getTable('sales/order_grid');
$gridQuoteOrderTables[] = $installer->getTable('sales/order');
$gridQuoteOrderTables[] = $installer->getTable('sales/quote');

$customerIpColumn = array(
    'customer_ip'
);
$columnParams = array (
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'nullable'  => true,
    'length' => 255,
    'comment'   => 'Customer IP',
);

Mage::log('before adding fields', null, $logName);

foreach ($gridQuoteOrderTables as $table2Update) {

    $tableFields = $connection->describeTable($table2Update);
    $fieldNames = array_keys($tableFields);
    $afterColumn = array_pop($fieldNames);
    $columnParams['after'] = $afterColumn;

    if (!$connection->tableColumnExists($table2Update, $customerIpColumn)) {
        try {
            $connection->addColumn($table2Update, $customerIpColumn, $columnParams);
            Mage::log('ADDED ' . $table2Update . ' to ' . $gridQuoteOrderTables, null, $logName);
        } catch (Exception $e) {
            Mage::log($table2Update . ' ' . $gridQuoteOrderTables . ' ' . $e->getCode() . ' ' . $e->getMessage(), null, $logName);
        }
    }
}
Mage::log('finish update', null, $logName);

$installer->endSetup();
