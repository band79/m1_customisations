<?php
/** @var $this Mage_Sales_Model_Resource_Setup */
$logName = 'order_grid_update.log';
Mage::log('start', null, $logName);

$installer = $this;
$installer->startSetup();

Mage::log('definitions', null, $logName);
$connection = $installer->getConnection();
$gridTable = $installer->getTable('sales/order_grid');
$orderTable = $installer->getTable('sales/order');
$fields = $connection->describeTable($gridTable);
$fieldNames = array_keys($fields);
$after = array_pop($fieldNames);

$columns = array(
    'super_store_id' => array (
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => true,
        'length' => 255,
        'comment'   => 'Super Store Id'
    ),
    'super_store_status' => array (
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => true,
        'length' => 255,
        'comment'   => 'Super Store Status'
    ),
);

Mage::log('before adding fields', null, $logName);

foreach ($columns as $columnName => $columnParams) {
    if (!$connection->tableColumnExists($gridTable, $columnName)) {
        try {
            $columnParams['after'] = $after;
            $connection->addColumn($gridTable, $columnName, $columnParams);
            $after = $columnName;
            Mage::log('ADDED ' . $columnName, null, 'order_grid_update.log');
        } catch (Exception $e) {
            Mage::log($columnName . ' ' . $e->getCode() . ' ' . $e->getMessage(), null, $logName);
        }
    }
}
Mage::log('after adding fields', null, $logName);

Mage::log('before updating fields', null, $logName);
$updateCollumns = array(
    'super_store_id',
    'super_store_status',
);
$select = $connection->select()->from(array('grid_table' => $gridTable), $updateCollumns);
$select->reset()->joinLeft(array('order_table' => $orderTable), 'grid_table.entity_id = order_table.entity_id', $updateCollumns);
$updateSql = $select->crossUpdateFromSelect(array('grid_table' => $gridTable));

$connection->beginTransaction();
try {
    Mage::log('updating fields start', null, $logName);
    $connection->query($updateSql);
    $connection->commit();
    Mage::log('updating fields commit', null, $logName);
} catch (Exception $e) {
    $connection->rollback();
    Mage::log('ERROR during update order grid table. ' . $e->getCode() . ' ' . $e->getMessage(), null, $logName);
}

Mage::log('updating fields end', null, $logName);

$installer->endSetup();
