<?php
/** @var $this Mage_Sales_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();

$installer->removeAttribute('order', 'super_store_error_message');
$installer->getConnection()->dropColumn(
    $installer->getTable('sales/order'),
    'super_store_error_message'
);
$installer->endSetup();
