<?php
/** @var $this Mage_Sales_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();

$attrText = array(
    'type' => Varien_Db_Ddl_Table::TYPE_VARCHAR
);

$installer->addAttribute('order', 'super_store_error_name', $attrText);
$installer->addAttribute('order', 'super_store_error_message', $attrText);

$installer->endSetup();
