<?php

$installer = $this;
$installer->startSetup();

/** @var Mage_Sales_Model_Resource_Setup Mage_Sales_Model_Resource_Setup */
$installer->addAttribute('quote', 'sku', array('type' => 'varchar'));
$installer->addAttribute('order', 'sku', array('type' => 'varchar'));

$installer->endSetup();
