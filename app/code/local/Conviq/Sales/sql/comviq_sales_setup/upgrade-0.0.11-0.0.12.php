<?php
/** @var $this Mage_Sales_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();

$attrBoolean = array(
    'type' => Varien_Db_Ddl_Table::TYPE_BOOLEAN
);
$attrText = array(
    'type' => Varien_Db_Ddl_Table::TYPE_VARCHAR
);

$installer->addAttribute('quote_item', 'varva_is_active', $attrBoolean);
$installer->addAttribute('order_item', 'varva_is_active', $attrBoolean);

$installer->addAttribute('quote_item', 'varva_code', $attrText);
$installer->addAttribute('order_item', 'varva_code', $attrText);

$installer->addAttribute('quote_item', 'varva_campaign_info', $attrText);
$installer->addAttribute('order_item', 'varva_campaign_info', $attrText);

$installer->endSetup();
