<?php

$installer = $this;
$installer->startSetup();

/** @var Mage_Sales_Model_Resource_Setup Mage_Sales_Model_Resource_Setup */
$installer->addAttribute('quote', 'student_data', array('type' => 'varchar'));
$installer->addAttribute('order', 'student_data', array('type' => 'varchar'));

$installer->endSetup();
