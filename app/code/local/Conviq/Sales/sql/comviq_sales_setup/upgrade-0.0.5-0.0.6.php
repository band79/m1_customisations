<?php
/** @var $this Mage_Sales_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();

$connection = $installer->getConnection();
$orderTable = $installer->getTable('sales/order');
$orderGridTable = $installer->getTable('sales/order_grid');

$status = Mage::getModel('sales/order_status');
$status
    ->setStatus('success')->setLabel('Ss4 success')
    ->assignState(Mage_Sales_Model_Order::STATE_NEW)
    ->save();
$status->unsetData()
    ->setStatus('unsuccess')->setLabel('Ss4 unsuccess')
    ->assignState(Mage_Sales_Model_Order::STATE_NEW)
    ->save();

$installer->run("
    update {$orderTable} set `status`='success' WHERE `status` <> 'success' and increment_id like 'TC%';
    update {$orderGridTable} set `status`='success' WHERE `status` <> 'success' and `increment_id` like 'TC%';
");

$installer->endSetup();