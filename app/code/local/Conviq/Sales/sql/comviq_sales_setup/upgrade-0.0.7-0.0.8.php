<?php
/** @var $this Mage_Sales_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();

/** @var Mage_Sales_Model_Resource_Setup Mage_Sales_Model_Resource_Setup */

$quoteTable = $installer->getTable('sales/quote_item');
$orderTable = $installer->getTable('sales/order_item');
$connection = $installer->getConnection();

$attributes = array (
    'subscription_type' => array(
        'attribute' => array('type' => 'varchar', 'size'=> '40', 'comment' => 'Subscription Type'),
        'db' => array('type' => 'varchar(40)', 'comment' => 'Subscription Type')
    ),
);

$installer->run("ALTER TABLE {$quoteTable} DROP COLUMN `subscription_type`;");
$installer->run("ALTER TABLE {$quoteTable} ADD COLUMN `subscription_type` varchar(40) NULL DEFAULT NULL COMMENT 'Subscription Type';");

$installer->run("ALTER TABLE {$orderTable} DROP COLUMN `subscription_type`;");
$installer->run("ALTER TABLE {$orderTable} ADD `subscription_type` varchar(40) NULL DEFAULT NULL COMMENT 'Subscription Type';");

$installer->endSetup();
