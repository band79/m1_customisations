<?php
/** @var $this Mage_Sales_Model_Resource_Setup */
$logName = 'order_grid_payment_update.log';
Mage::log('start update', null, $logName);

$installer = $this;
$installer->startSetup();

$connection = $installer->getConnection();
$paymentTable = $installer->getTable('sales/order_payment');
$gridTable = $installer->getTable('sales/order_grid');
$columnName = 'payment_method_code';
$columnParams = array (
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'nullable'  => true,
    'length' => 255,
    'comment'   => 'Payment Method Code',
    'after'   => 'shipping_method',
);

if (!$connection->tableColumnExists($gridTable, $columnName)) {
    $connection->addColumn($gridTable, $columnName, $columnParams);
}

$collumns = array('payment_method_code'=>'method');
$select = $connection->select()->from(array('grid_table' => $gridTable), array('grid_table.payment_method_code'));
$select->reset()
    ->joinLeft(array('payment_table' => $paymentTable), 'grid_table.entity_id = payment_table.parent_id', $collumns);
$updateSql = $select->crossUpdateFromSelect(array('grid_table' => $gridTable));

$connection->beginTransaction();
try {
    $connection->query($updateSql);
    $connection->commit();
} catch (Exception $e) {
    $connection->rollback();
    Mage::log('ERROR during update order grid table. ' . $e->getCode() . ' ' . $e->getMessage(), null, $logName);
}

Mage::log('finish update', null, $logName);

$installer->endSetup();
