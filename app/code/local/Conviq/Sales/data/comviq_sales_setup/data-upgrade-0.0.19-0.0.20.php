<?php
/** @var $this Mage_Sales_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();

$cmsBlocks = array(
    array(
        'title'         => 'Print page POSTEN Text',
        'identifier'    => 'print_page_posten_text',
        'content'       => 'Vi hör av oss via SMS om det sker några ändringar gällande din leveranstid. Du kan även gå in på {{customVar code=cv_my_account}} för att hålla koll på din beställning',
        'is_active'     => 1,
        'stores'        => 0
    ),
    array(
        'title'         => 'Email POSTEN Text',
        'identifier'    => 'email_posten_text',
        'content'       => 'Vi hör av oss via SMS om det sker några ändringar gällande din leveranstid. Du kan även gå in på {{customVar code=cv_my_account}} för att hålla koll på din beställning.',
        'is_active'     => 1,
        'stores'        => 0
    ),
    array(
        'title'         => 'Print page Leveranstid',
        'identifier'    => 'print_page_delivery_time',
        'content'       => '<strong>Leveranstid</strong><br/>%s<br/>Vi hör av oss via SMS om det sker några ändringar gällande din leveranstid. Du kan även gå in på %s för att hålla koll på din beställning',
        'is_active'     => 1,
        'stores'        => 0
    )
);

$customVariables = array(
    array(
        'code'          => 'cv_my_account',
        'name'          => 'Mitt konto',
        'html_value'    => '<a href="https://api.comviq.se/v3/oauth/login?client_id=comviq_se&scope=openid&state=u4DDigDTabHW8fusHLuDOQ%3d%3d-%2bNDr9brKkYPNarl3FgZrVAhudT3hOmhtsODzPdbKhBji%2bSsLG84%2fnmnizzD4ZQXGtP8fj91CwYY3IHAtcv5hOvH0cLWdsANfpZhW6x7OIqBPjh%2bJzc3UcvkwfiYPODbOu2xQ9tkjFz1FOFe2k7ZIgSGNeIgWdq2e6NLSHAlrtZq0tYajk%2bUMx1aZR7GGQl9Cymkc%2f3caqOPjBQ997LFPpYDbWWBEgN2GSMa6eM3rRGnzPJ5drOZAVSAX9lxpWL3OA%2fl0CwQE2wsTfOgOH3UN7927geI2CusIT%2fLGbuGwAaE1paN%2bBrr82nSupQ8Cw7CbBwWZ6cFKngHKMMbCsOHWIBOSM7mnit%2bZd5Cui8Nfj%2f2aqoM81DoUOon4%2fFm6MubBsv9mZnvdMzTdcAGkQMhw%2f5uJ5Lzy%2bFNJLokrHnvhTEs8HAbnmUzP5NCCvDAEAxeL">Mitt konto</a>',
    ),
    array(
        'code'          => 'cv_print_page_total_monthly_price',
        'name'          => 'Totalt månadspris',
        'html_value'    => 'Totalt månadspris %s kr/mån',
    ),
    array(
        'code'          => 'cv_print_page_pay_now',
        'name'          => 'Betala nu',
        'html_value'    => 'Betala nu %s kr <br/>',
    ),
    array(
        'code'          => 'cv_print_page_discount',
        'name'          => 'Rabatt',
        'html_value'    => 'Rabatt %s kr <br/>',
        'plain_value'   => 'Rabatt %s kr',
    ),
    array(
        'code'          => 'cv_email_togo_butik',
        'name'          => 'Vald butik',
        'html_value'    => 'Utlämning i butik.<br />Vald butik:<br />',
    ),
    array(
        'code'          => 'cv_email_personal_information',
        'name'          => 'Personuppgifter',
        'html_value'    => 'Personuppgifter:',
    ),
    array(
        'code'          => 'cv_email_payment_method',
        'name'          => 'Betalningssätt',
        'html_value'    => 'Betalningssätt:'
    ),
    array(
        'code'          => 'cv_email_shipping_Information',
        'name'          => 'Leveransinformation',
        'html_value'    => 'Leveransinformation:'
    ),
    array(
        'code'          => 'cv_email_delivery',
        'name'          => 'Leveranssätt',
        'html_value'    => 'Leveranssätt:'
    ),
    array(
        'code'          => 'cv_email_delivery_address',
        'name'          => 'Leveransadress',
        'html_value'    => '<br><b>Leveransadress:</b><br>'
    ),
    array(
        'code'          => 'cv_email_product',
        'name'          => 'Produkt',
        'html_value'    => 'Produkt'
    ),
    array(
        'code'          => 'cv_email_qty',
        'name'          => 'Antal',
        'html_value'    => 'Antal'
    ),
    array(
        'code'          => 'cv_email_pay_now',
        'name'          => 'Betalas nu',
        'html_value'    => 'Betalas nu'
    ),
    array(
        'code'          => 'cv_email_per_month',
        'name'          => 'Per månad',
        'html_value'    => 'Per månad'
    ),
    array(
        'code'          => 'cv_email_total',
        'name'          => 'Total',
        'html_value'    => '<strong>Total:</strong>'
    ),
    array(
        'code'          => 'cv_email_discount',
        'name'          => 'Rabatt',
        'html_value'    => '<strong>Rabatt:</strong>'
    )
);

$cvModel = Mage::getModel('core/variable');
foreach ($customVariables as $data) {
    $cvModel->unsetData();
    if (!$cvModel->load($data['code'], 'code')->getId()) {
        $cvModel->setData($data)->save();
    }
}

$model = Mage::getModel('cms/block');
foreach ($cmsBlocks as $data) {
    $model->unsetData();
    if (!$model->load($data['identifier'], 'identifier')->getId()) {
        $model->setData($data)->save();
    }
}
$installer->endSetup();