<?php
/** @var $this Mage_Sales_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();


$customVariables = array(
    array(
        'code'          => 'cv_success_page_terms',
        'name'          => 'Success Page Terms',
        'html_value'    => '<strong>VILLKOR</strong>'
    ),
);

$cvModel = Mage::getModel('core/variable');
foreach ($customVariables as $data) {
    $cvModel->unsetData();
    if (!$cvModel->load($data['code'], 'code')->getId()) {
        $cvModel->setData($data)->save();
    }
}

$installer->endSetup();
