<?php
/** @var $this Mage_Sales_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();


$customVariables = array(
    array(
        'code'          => 'cv_success_page_title',
        'name'          => 'Success Page Title',
        'html_value'    => 'Tack för att du väljer Comviq!'
    ),
    array(
        'code'          => 'cv_success_page_mobile',
        'name'          => 'Success Page Mobile',
        'html_value'    => '<strong class="text-uppercase">MOBIL</strong>'
    ),
    array(
        'code'          => 'cv_success_page_accessories',
        'name'          => 'Success Page Accessories',
        'html_value'    => '<strong class="text-uppercase">TILLBEHÖR</strong>'
    ),
    array(
        'code'          => 'cv_success_page_kontantkort',
        'name'          => 'Success Page Kontantkort',
        'html_value'    => '<strong class="text-uppercase">Kontantkort</strong>'
    ),
    array(
        'code'          => 'cv_success_page_abonnemang',
        'name'          => 'Success Page Abonnemang',
        'html_value'    => '<strong class="text-uppercase">Abonnemang</strong>'
    ),
    array(
        'code'          => 'cv_success_page_installment',
        'name'          => 'Success Page Installment',
        'html_value'    => '<strong class="text-uppercase">Avbetalning</strong><p>%s man for mobilen</p>'
    ),
    array(
        'code'          => 'cv_success_page_price',
        'name'          => 'Success Page Price',
        'html_value'    => '<strong class="text-uppercase">Pris</strong>'
    ),
    array(
        'code'          => 'cv_success_page_order_number',
        'name'          => 'Success Page Order Number',
        'html_value'    => '<strong class="text-uppercase">Ordernr</strong>'
    ),
    array(
        'code'          => 'cv_success_page_order_date',
        'name'          => 'Success Page Order Date',
        'html_value'    => '<strong class="text-uppercase">Datum</strong>'
    ),
    array(
        'code'          => 'cv_success_page_receipt',
        'name'          => 'Success Page Receipt',
        'html_value'    => '<strong class="text-uppercase">Kvitto</strong><p><a href="%s" target="_blank">LADDA HEM KVITTO</a></p>'
    ),
    array(
        'code'          => 'cv_success_page_delivery',
        'name'          => 'Success Page Delivery',
        'html_value'    => '<strong class="text-uppercase">LEVERANS</strong>'
    ),
    array(
        'code'          => 'cv_success_page_delivery',
        'name'          => 'Success Page Delivery',
        'html_value'    => '<strong class="text-uppercase">LEVERANS</strong>'
    ),
    array(
        'code'          => 'cv_success_page_confirmation_email',
        'name'          => 'Success Page Confirmation Email',
        'html_value'    => 'Vi har skickat ett bekräftelsemail till: %s.'
    ),
    array(
        'code'          => 'cv_success_page_bindperiod',
        'name'          => 'Success Page Bind Period',
        'html_value'    => ',<br/> %s mån bindningstid'
    ),
    array(
        'code'          => 'cv_success_page_pay_now',
        'name'          => 'Success Page Pay Now',
        'html_value'    => 'Pay now %s kr <br/>'
    ),
);

$cvModel = Mage::getModel('core/variable');
foreach ($customVariables as $data) {
    $cvModel->unsetData();
    if (!$cvModel->load($data['code'], 'code')->getId()) {
        $cvModel->setData($data)->save();
    }
}

$installer->endSetup();
