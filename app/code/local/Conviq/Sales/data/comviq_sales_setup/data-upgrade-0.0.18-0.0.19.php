<?php
/** @var $this Mage_Sales_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();

$cmsBlocks = array(
    array(
        'title'         => 'Success page POSTEN Text',
        'identifier'    => 'success_page_posten_text',
        'content'       => 'Din beställning skickas till din folkbokföringsadress. Vi hör av oss via SMS om det sker några ändringar gällande din leveranstid. Du kan även gå in på <a href="https://api.comviq.se/v3/oauth/login?client_id=comviq_se&scope=openid&state=u4DDigDTabHW8fusHLuDOQ%3d%3d-%2bNDr9brKkYPNarl3FgZrVAhudT3hOmhtsODzPdbKhBji%2bSsLG84%2fnmnizzD4ZQXGtP8fj91CwYY3IHAtcv5hOvH0cLWdsANfpZhW6x7OIqBPjh%2bJzc3UcvkwfiYPODbOu2xQ9tkjFz1FOFe2k7ZIgSGNeIgWdq2e6NLSHAlrtZq0tYajk%2bUMx1aZR7GGQl9Cymkc%2f3caqOPjBQ997LFPpYDbWWBEgN2GSMa6eM3rRGnzPJ5drOZAVSAX9lxpWL3OA%2fl0CwQE2wsTfOgOH3UN7927geI2CusIT%2fLGbuGwAaE1paN%2bBrr82nSupQ8Cw7CbBwWZ6cFKngHKMMbCsOHWIBOSM7mnit%2bZd5Cui8Nfj%2f2aqoM81DoUOon4%2fFm6MubBsv9mZnvdMzTdcAGkQMhw%2f5uJ5Lzy%2bFNJLokrHnvhTEs8HAbnmUzP5NCCvDAEAxeL">Mitt konto</a> för att hålla koll på din beställning.',
        'is_active'     => 1,
        'stores'        => 0
    ),

    array(
        'title'         => 'Success page TOGO Text',
        'identifier'    => 'success_page_togo_text',
        'content'       => 'Hämtas direkt på',
        'is_active'     => 1,
        'stores'        => 0
    ),
);

$model = Mage::getModel('cms/block');
foreach ($cmsBlocks as $data) {
    $model->unsetData();
    if (!$model->load($data['identifier'], 'identifier')->getId()) {
        $model->setData($data)->save();
    }
}
$installer->endSetup();