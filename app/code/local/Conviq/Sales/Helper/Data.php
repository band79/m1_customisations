<?php
class Comviq_Sales_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected $_alterDeliveryText = array();
    protected $_checkoutHelper = null;

    public function getComviqCheckoutHelper()
    {
        if (!isset($this->_checkoutHelper)) {
            /* @var $_checkoutHelper Comviq_Checkout_Helper_Data */
            $_checkoutHelper = Mage::helper('comviq_checkout');
            $this->_checkoutHelper = $_checkoutHelper;
        }

        return $this->_checkoutHelper;
    }

    public function getAlternativeExpectedDelivery($order)
    {
        if (!$order->getId()) {
            return false;
        }

        if (!isset($this->_alterDeliveryText[$order->getId()])) {
            $items = $order->getItemsCollection();
            $this->_alterDeliveryText[$order->getId()] = '';
            foreach ($items as $_item) {
                if ($_item->getProduct()->getAlternativeDeliveryTime()) {
                    $this->_alterDeliveryText[$order->getId()] = $_item->getProduct()
                        ->getAlternativeDeliveryTime();
                    break;
                }
            }
        }
        return $this->_alterDeliveryText[$order->getId()];
    }

    /**
     * @param $order
     *
     * @return mixed|null
     * @throws Comviq_Checkout_CheckoutException
     */
    public function getExpectedDeliveryText($order)
    {
        $subsOwnerItem = $this->getComviqCheckoutHelper()->getSubscriptionOwnerItemFromOrder($order);
        $expDeliveryTime = $subsOwnerItem->getData('expected_delivery_time');
        if ($this->getAlternativeExpectedDelivery($order)) {
            $text = Mage::app()->getLayout()
                ->createBlock('cms/block')
                ->setBlockId('confirmation_msg_posten_alternative')
                ->toHtml();
        } else {
            $text = Mage::app()->getLayout()
                ->createBlock('cms/block')
                ->setBlockId('confirmation_msg_posten')
                ->toHtml();
        }
        $text = str_replace('{EXPECTED_TIME}', $expDeliveryTime, $text);
        /* @var $comviqCatalogHelper Comviq_Catalog_Helper_Data */
        $comviqCatalogHelper = Mage::helper('comviq_catalog');
        $product = $subsOwnerItem->getProduct();
        if ($comviqCatalogHelper->isMobile($product)) {
            $subsOwnerName = $this->__('Din') . ' ' .
                $subsOwnerItem->getData('product_make') . ' ' .
                $subsOwnerItem->getParentItem()->getData('name');
        } else {
            $mobilePost = array(
                Comviq_Subscription_Model_Mobile::MOBILE_VOICE_POSTPAID,
                Comviq_Subscription_Model_Mobile::MOBILE_BROADBAND_POSTPAID
            );
            if (in_array($subsOwnerItem->getSubscriptionType(), $mobilePost)) {
                $subsOwnerName = $this->__('Ditt') . ' ' . $this->__('SIM-kort');
            } else {
                $subsOwnerName = $this->__('Ditt') . ' ' . $this->__('startpaket');
            }
        }
        $text = str_replace('{SUBSCRIPTION_OWNER_NAME}', $subsOwnerName, $text);

        return $text;
    }

    /**
     * Save offer data to the order
     *
     *
     * @param      $order
     * @param      $data
     * @param bool $flagReplace
     *
     * @return mixed|null
     */
    public function setOrderOfferData($order, $data, $flagReplace = false)
    {
        if (is_array($data) && count($data)) {
            if ($flagReplace) {
                $offerData = $data;
            } else {
                if ($orderOfferData = $order->getOfferData()) {
                    $orderOfferData = unserialize($orderOfferData);
                }
                $orderOfferData = (array)$orderOfferData;
                $offerData = array_merge($orderOfferData, $data);
            }
            $order->setOfferData(serialize($offerData))
                ->setNeedSave(true);
            return true;
        }
        return false;
    }
}
